-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 19-Mar-2019 às 05:18
-- Versão do servidor: 10.3.13-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id8836933_cmm`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `area_nome` varchar(200) DEFAULT NULL,
  `area_pos` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `area`
--

INSERT INTO `area` (`area_id`, `area_nome`, `area_pos`) VALUES
(1, 'Marketing', 1),
(2, 'NegÃ³cios', 0),
(3, 'Novidades', 0),
(4, 'ServiÃ§os', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `area1`
--

CREATE TABLE `area1` (
  `area1_id` int(11) NOT NULL,
  `area1_nome` varchar(200) DEFAULT NULL,
  `area1_pos` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `area1`
--

INSERT INTO `area1` (`area1_id`, `area1_nome`, `area1_pos`) VALUES
(1, 'SISTEMA WEB', 0),
(2, 'SITES', 1),
(3, 'APLICATIVOS', 2),
(4, 'CONSULTORIA', 3),
(5, 'TESTE', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `area2`
--

CREATE TABLE `area2` (
  `area2_id` int(11) NOT NULL,
  `area2_nome` varchar(200) DEFAULT NULL,
  `area2_pos` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `area2`
--

INSERT INTO `area2` (`area2_id`, `area2_nome`, `area2_pos`) VALUES
(1, 'MotivaÃ§Ã£o', 0),
(2, 'NegÃ³cios', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `area3`
--

CREATE TABLE `area3` (
  `area3_id` int(11) NOT NULL,
  `area3_nome` varchar(200) CHARACTER SET utf8 NOT NULL,
  `area3_parent` int(11) NOT NULL DEFAULT 0,
  `area3_pos` int(11) NOT NULL DEFAULT 0,
  `area3_level` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `area3`
--

INSERT INTO `area3` (`area3_id`, `area3_nome`, `area3_parent`, `area3_pos`, `area3_level`) VALUES
(1, 'Tecnologia', 0, 1, 1),
(2, 'Monitoramento', 1, 0, 0),
(3, 'ServiÃ§os', 0, 0, 1),
(4, 'Alarme', 1, 0, 0),
(5, 'Jardinagem', 3, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros`
--

CREATE TABLE `cadastros` (
  `cadastro_id` int(11) NOT NULL,
  `cadastro_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cadastro_data` date NOT NULL,
  `cadastro_status` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cadastros`
--

INSERT INTO `cadastros` (`cadastro_id`, `cadastro_email`, `cadastro_data`, `cadastro_status`) VALUES
(1, 'email@dominio.com', '2017-11-11', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `cliente_id` int(11) NOT NULL,
  `cliente_nome` varchar(200) DEFAULT NULL,
  `cliente_subtitulo` varchar(200) DEFAULT NULL,
  `cliente_descricao` text NOT NULL,
  `cliente_imagem` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `cliente_nome`, `cliente_subtitulo`, `cliente_descricao`, `cliente_imagem`) VALUES
(1, 'Microsoft', 'https://www.microsoft.com/pt-br', '', '1506974107.png'),
(2, 'Samsung', 'http://www.samsung.com/br/', '', '1506977386.png'),
(3, 'YouTube', 'https://www.youtube.com', '', '1507407107.jpg'),
(4, 'Facebook', 'https://www.facebook.com', '', '1507406749.png'),
(5, 'Microsoft', 'https://www.microsoft.com/pt-br', '', '1507407174.png'),
(6, 'Samsung', 'http://www.samsung.com/br/', '', '1507407198.png'),
(7, 'YouTube', 'https://www.youtube.com', '', '1507407219.jpg'),
(8, 'Facebook', 'https://www.facebook.com', '', '1507407235.png'),
(9, 'Lata Guetto Artes', 'https://lataguettoartes.000webhostapp.com/', '', '1552704249.png'),
(10, 'Açai Number One', 'https://pointacainumberone.herokuapp.com/cardapio', '', '1552704881.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE `comentario` (
  `comentario_id` int(11) NOT NULL,
  `comentario_nome` varchar(200) DEFAULT NULL,
  `comentario_email` varchar(200) DEFAULT NULL,
  `comentario_mensagem` text DEFAULT NULL,
  `comentario_data` varchar(200) DEFAULT NULL,
  `comentario_status` int(11) DEFAULT 0,
  `comentario_pagina` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `contato_id` int(11) NOT NULL,
  `contato_email` varchar(200) DEFAULT NULL,
  `contato_telefone1` varchar(200) DEFAULT NULL,
  `contato_endereco` varchar(200) DEFAULT NULL,
  `contato_maps` text DEFAULT NULL,
  `contato_long_lat` varchar(200) DEFAULT NULL,
  `contato_telefone2` varchar(200) DEFAULT NULL,
  `contato_telefone3` varchar(200) DEFAULT NULL,
  `contato_telefone4` varchar(200) DEFAULT NULL,
  `contato_telefone5` varchar(200) DEFAULT NULL,
  `contato_telefone6` varchar(200) DEFAULT NULL,
  `contato_horario` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`contato_id`, `contato_email`, `contato_telefone1`, `contato_endereco`, `contato_maps`, `contato_long_lat`, `contato_telefone2`, `contato_telefone3`, `contato_telefone4`, `contato_telefone5`, `contato_telefone6`, `contato_horario`) VALUES
(1, 'contato@cmmsoftware.com.br', '(71) 8121-8522', 'Salvador - BA', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62218.34167951526!2d-38.537298073945635!3d-12.930436320061348!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7160f8afc4458ad%3A0x2e95c09c7580186c!2sBas%C3%ADlica+do+Senhor+do+Bonfim!5e0!3m2!1spt-BR!2sbr!4v1551309256242\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '--23.5692979,-46.6467303', '', '', '', '', '', 'Seg - Dom: 09:00 - 18:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `depoimento`
--

CREATE TABLE `depoimento` (
  `depoimento_id` int(11) NOT NULL,
  `depoimento_nome` varchar(200) DEFAULT NULL,
  `depoimento_cargo` varchar(200) DEFAULT NULL,
  `depoimento_sobre` text DEFAULT NULL,
  `depoimento_data` varchar(200) DEFAULT NULL,
  `depoimento_imagem` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `depoimento`
--

INSERT INTO `depoimento` (`depoimento_id`, `depoimento_nome`, `depoimento_cargo`, `depoimento_sobre`, `depoimento_data`, `depoimento_imagem`) VALUES
(1, 'Bruna', 'Sorocaba/SP', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', '08/08/2017', '1551308679.jpg'),
(2, 'Cristiano', 'Curitiba/PR', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.', '05/10/2017', '1507232111.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipe`
--

CREATE TABLE `equipe` (
  `equipe_id` int(11) NOT NULL,
  `equipe_nome` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_subtitulo` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_descricao` text CHARACTER SET utf8 NOT NULL,
  `equipe_imagem` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_link1` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_link2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_link3` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `equipe_link4` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `equipe`
--

INSERT INTO `equipe` (`equipe_id`, `equipe_nome`, `equipe_subtitulo`, `equipe_descricao`, `equipe_imagem`, `equipe_link1`, `equipe_link2`, `equipe_link3`, `equipe_link4`) VALUES
(1, 'Celso Barreto', 'DIR. ADM/FINANCEIRO', 'É desenvolvedor e Analista de Sistemas, formado em Administração com Análise de Sistemas.', '1551229268.png', 'email@email.com', 'https://www.twitter.com', 'https://www.facebook.com', 'https://br.linkedin.com/'),
(2, 'Alisson Costa', 'DIR. TECNOLOGIA E INOVAÇÃO', 'É desenvolvedor e Analista de Sistemas formado em Ciência da Computação.', '1551229342.png', 'teste@teste.com', 'https://www.twitter.com', 'https://www.facebook.com', 'https://br.linkedin.com/'),
(3, 'Robson Nascimento', 'DIR. ESTRATÉGICO E COMERCIAL', 'É desenvolvedor, formado em Administração e especialista em Gestão Comercial.', '1551229565.png', 'teste@teste.com', 'https://www.twitter.com', 'https://www.facebook.com', 'https://br.linkedin.com/'),
(4, 'Luan Bispo', 'APOIO E DESENVOLVIMENTO', 'É desenvolvedor, formado em Desenvolvimento de Sistemas.', '1551229525.png', 'teste@teste.com', 'https://www.twitter.com', 'https://www.facebook.com', 'https://br.linkedin.com/');

-- --------------------------------------------------------

--
-- Estrutura da tabela `foto`
--

CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL,
  `foto_url` varchar(200) NOT NULL,
  `foto_pos` int(11) DEFAULT 0,
  `foto_portfolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `foto`
--

INSERT INTO `foto` (`foto_id`, `foto_url`, `foto_pos`, `foto_portfolio`) VALUES
(1, 'ac276e73d382d5d88c0151d165b98433.jpg', 0, 1),
(2, '9014d739f45ca4c406271e338bd877d4.jpg', 0, 1),
(3, '44c9d0aca5394df0376b3145d30fd5e3.jpg', 0, 1),
(7, '03ed1af071b07b811a105cc0e857008e.jpg', 0, 3),
(8, '141dfabdd512c381d05be94beaecce4e.jpg', 0, 3),
(9, '14a62737e8a6a8934370f965ac70226b.jpg', 0, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE `fotos` (
  `fotos_id` int(11) NOT NULL,
  `fotos_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `fotos_pos` int(11) NOT NULL DEFAULT 0,
  `fotos_paginas` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`fotos_id`, `fotos_url`, `fotos_pos`, `fotos_paginas`) VALUES
(4, 'e6e649460b33a9ef6e87acfb764fa830.jpg', 0, 2),
(11, 'd21369d340030b342ec09fb0d7fcaac2.jpg', 0, 7),
(9, 'f88ad913a92103ced9b5eaa6567377c7.jpg', 0, 7),
(12, 'dcb8f3905af108850723c334ebcd9212.jpg', 0, 7),
(13, 'b21efa45b66c7138d980beeb270cac9f.jpg', 0, 7),
(14, 'ab1e734c06e68a648071afa9a5f5e9aa.jpg', 0, 7),
(15, 'b188152b314f8e372506522f7eab0bdc.jpg', 0, 7),
(72, '9a951e3192ac21e01bca1e6d90f1c4b6.jpg', 0, 8),
(71, 'b6872599dd7fa4205c4da73116af33fd.jpg', 0, 8),
(70, 'ff7de266975fb28dd1ba3f6c8497fac1.jpg', 0, 8),
(67, '4ce777301bb954d13fd758b20d5b57de.jpg', 0, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `icones`
--

CREATE TABLE `icones` (
  `icones_id` int(11) NOT NULL,
  `icones_nome` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `icones`
--

INSERT INTO `icones` (`icones_id`, `icones_nome`) VALUES
(1, 'fa fa-bed'),
(2, 'fa fa-buysellads'),
(3, 'fa fa-cart-arrow-down'),
(4, 'fa fa-cart-arrow-down'),
(5, 'fa fa-connectdevelop'),
(6, 'fa fa-diamond'),
(7, 'fa fa-facebook-official'),
(8, 'fa fa-forumbee'),
(9, 'fa fa-hotel'),
(10, 'fa fa-leanpub'),
(11, 'fa fa-mars'),
(12, 'fa fa-mars-double'),
(13, 'fa fa-mars-stroke'),
(14, 'fa fa-mars-stroke-h'),
(15, 'fa fa-mars-stroke-v'),
(16, 'fa fa-medium'),
(17, 'fa fa-mercury'),
(18, 'fa fa-motorcycle'),
(19, 'fa fa-neuter'),
(20, 'fa fa-pinterest-p'),
(21, 'fa fa-sellsy'),
(22, 'fa fa-server'),
(23, 'fa fa-ship'),
(24, 'fa fa-shirtsinbulk'),
(25, 'fa fa-simplybuilt'),
(26, 'fa fa-skyatlas'),
(27, 'fa fa-street-view'),
(28, 'fa fa-subway'),
(29, 'fa fa-train'),
(30, 'fa fa-transgender'),
(31, 'fa fa-transgender-alt'),
(32, 'fa fa-user-plus'),
(33, 'fa fa-user-secret'),
(34, 'fa fa-user-times'),
(35, 'fa fa-venus'),
(36, 'fa fa-venus-double'),
(37, 'fa fa-venus-mars'),
(38, 'fa fa-viacoin'),
(39, 'fa fa-whatsapp'),
(40, 'fa fa-adjust'),
(41, 'fa fa-anchor'),
(42, 'fa fa-archive'),
(43, 'fa fa-area-chart'),
(44, 'fa fa-arrows'),
(45, 'fa fa-arrows-h'),
(46, 'fa fa-arrows-v'),
(47, 'fa fa-asterisk'),
(48, 'fa fa-at'),
(49, 'fa fa-automobile'),
(50, 'fa fa-ban'),
(51, 'fa fa-bank'),
(52, 'fa fa-bar-chart'),
(53, 'fa fa-bar-chart-o'),
(54, 'fa fa-barcode'),
(55, 'fa fa-bars'),
(56, 'fa fa-bed'),
(57, 'fa fa-beer'),
(58, 'fa fa-bell'),
(59, 'fa fa-bell-o'),
(60, 'fa fa-bell-slash'),
(61, 'fa fa-bell-slash-o'),
(62, 'fa fa-bicycle'),
(63, 'fa fa-binoculars'),
(64, 'fa fa-birthday-cake'),
(65, 'fa fa-bolt'),
(66, 'fa fa-bomb'),
(67, 'fa fa-book'),
(68, 'fa fa-bookmark'),
(69, 'fa fa-bookmark-o'),
(70, 'fa fa-briefcase'),
(71, 'fa fa-bug'),
(72, 'fa fa-building'),
(73, 'fa fa-building-o'),
(74, 'fa fa-bullhorn'),
(75, 'fa fa-bullseye'),
(76, 'fa fa-bus'),
(77, 'fa fa-cab'),
(78, 'fa fa-calculator'),
(79, 'fa fa-calendar'),
(80, 'fa fa-calendar-o'),
(81, 'fa fa-camera'),
(82, 'fa fa-camera-retro'),
(83, 'fa fa-car'),
(84, 'fa fa-caret-square-o-down'),
(85, 'fa fa-caret-square-o-left'),
(86, 'fa fa-caret-square-o-right'),
(87, 'fa fa-caret-square-o-up'),
(88, 'fa fa-cart-arrow-down'),
(89, 'fa fa-cart-plus'),
(90, 'fa fa-cc'),
(91, 'fa fa-certificate'),
(92, 'fa fa-check'),
(93, 'fa fa-check-circle'),
(94, 'fa fa-check-circle-o'),
(95, 'fa fa-check-square'),
(96, 'fa fa-check-square-o'),
(97, 'fa fa-child'),
(98, 'fa fa-circle'),
(99, 'fa fa-circle-o'),
(100, 'fa fa-circle-o-notch'),
(101, 'fa fa-circle-thin'),
(102, 'fa fa-clock-o'),
(103, 'fa fa-close'),
(104, 'fa fa-cloud'),
(105, 'fa fa-cloud-download'),
(106, 'fa fa-cloud-upload'),
(107, 'fa fa-code'),
(108, 'fa fa-code-fork'),
(109, 'fa fa-coffee'),
(110, 'fa fa-cog'),
(111, 'fa fa-cogs'),
(112, 'fa fa-comment'),
(113, 'fa fa-comment-o'),
(114, 'fa fa-comments'),
(115, 'fa fa-comments-o'),
(116, 'fa fa-compass'),
(117, 'fa fa-copyright'),
(118, 'fa fa-credit-card'),
(119, 'fa fa-crop'),
(120, 'fa fa-crosshairs'),
(121, 'fa fa-cube'),
(122, 'fa fa-cubes'),
(123, 'fa fa-cutlery'),
(124, 'fa fa-dashboard'),
(125, 'fa fa-database'),
(126, 'fa fa-desktop'),
(127, 'fa fa-diamond'),
(128, 'fa fa-dot-circle-o'),
(129, 'fa fa-download'),
(130, 'fa fa-edit'),
(131, 'fa fa-ellipsis-h'),
(132, 'fa fa-ellipsis-v'),
(133, 'fa fa-envelope'),
(134, 'fa fa-envelope-o'),
(135, 'fa fa-envelope-square'),
(136, 'fa fa-eraser'),
(137, 'fa fa-exchange'),
(138, 'fa fa-exclamation'),
(139, 'fa fa-exclamation-circle'),
(140, 'fa fa-exclamation-triangle'),
(141, 'fa fa-external-link'),
(142, 'fa fa-external-link-square'),
(143, 'fa fa-eye'),
(144, 'fa fa-eye-slash'),
(145, 'fa fa-eyedropper'),
(146, 'fa fa-fax'),
(147, 'fa fa-female'),
(148, 'fa fa-fighter-jet'),
(149, 'fa fa-file-archive-o'),
(150, 'fa fa-file-audio-o'),
(151, 'fa fa-file-code-o'),
(152, 'fa fa-file-excel-o'),
(153, 'fa fa-file-image-o'),
(154, 'fa fa-file-movie-o'),
(155, 'fa fa-file-pdf-o'),
(156, 'fa fa-file-photo-o'),
(157, 'fa fa-file-picture-o'),
(158, 'fa fa-file-powerpoint-o'),
(159, 'fa fa-file-sound-o'),
(160, 'fa fa-file-video-o'),
(161, 'fa fa-file-word-o'),
(162, 'fa fa-file-zip-o'),
(163, 'fa fa-film'),
(164, 'fa fa-filter'),
(165, 'fa fa-fire'),
(166, 'fa fa-fire-extinguisher'),
(167, 'fa fa-flag'),
(168, 'fa fa-flag-checkered'),
(169, 'fa fa-flag-o'),
(170, 'fa fa-flash'),
(171, 'fa fa-flask'),
(172, 'fa fa-folder'),
(173, 'fa fa-folder-o'),
(174, 'fa fa-folder-open'),
(175, 'fa fa-folder-open-o'),
(176, 'fa fa-frown-o'),
(177, 'fa fa-futbol-o'),
(178, 'fa fa-gamepad'),
(179, 'fa fa-gavel'),
(180, 'fa fa-gear'),
(181, 'fa fa-gears'),
(182, 'fa fa-genderless'),
(183, 'fa fa-gift'),
(184, 'fa fa-glass'),
(185, 'fa fa-globe'),
(186, 'fa fa-graduation-cap'),
(187, 'fa fa-group'),
(188, 'fa fa-hdd-o'),
(189, 'fa fa-headphones'),
(190, 'fa fa-heart'),
(191, 'fa fa-heart-o'),
(192, 'fa fa-heartbeat'),
(193, 'fa fa-history'),
(194, 'fa fa-home'),
(195, 'fa fa-hotel'),
(196, 'fa fa-image'),
(197, 'fa fa-inbox'),
(198, 'fa fa-info'),
(199, 'fa fa-info-circle'),
(200, 'fa fa-institution'),
(201, 'fa fa-key'),
(202, 'fa fa-keyboard-o'),
(203, 'fa fa-language'),
(204, 'fa fa-laptop'),
(205, 'fa fa-leaf'),
(206, 'fa fa-legal'),
(207, 'fa fa-lemon-o'),
(208, 'fa fa-level-down'),
(209, 'fa fa-level-up'),
(210, 'fa fa-life-bouy'),
(211, 'fa fa-life-buoy'),
(212, 'fa fa-life-ring'),
(213, 'fa fa-life-saver'),
(214, 'fa fa-lightbulb-o'),
(215, 'fa fa-line-chart'),
(216, 'fa fa-location-arrow'),
(217, 'fa fa-lock'),
(218, 'fa fa-magic'),
(219, 'fa fa-magnet'),
(220, 'fa fa-mail-forward'),
(221, 'fa fa-mail-reply'),
(222, 'fa fa-mail-reply-all'),
(223, 'fa fa-male'),
(224, 'fa fa-map-marker'),
(225, 'fa fa-meh-o'),
(226, 'fa fa-microphone'),
(227, 'fa fa-microphone-slash'),
(228, 'fa fa-minus'),
(229, 'fa fa-minus-circle'),
(230, 'fa fa-minus-square'),
(231, 'fa fa-minus-square-o'),
(232, 'fa fa-mobile'),
(233, 'fa fa-mobile-phone'),
(234, 'fa fa-money'),
(235, 'fa fa-moon-o'),
(236, 'fa fa-mortar-board'),
(237, 'fa fa-motorcycle'),
(238, 'fa fa-music'),
(239, 'fa fa-navicon'),
(240, 'fa fa-newspaper-o'),
(241, 'fa fa-paint-brush'),
(242, 'fa fa-paper-plane'),
(243, 'fa fa-paper-plane-o'),
(244, 'fa fa-paw'),
(245, 'fa fa-pencil'),
(246, 'fa fa-pencil-square'),
(247, 'fa fa-pencil-square-o'),
(248, 'fa fa-phone'),
(249, 'fa fa-phone-square'),
(250, 'fa fa-photo'),
(251, 'fa fa-picture-o'),
(252, 'fa fa-pie-chart'),
(253, 'fa fa-plane'),
(254, 'fa fa-plug'),
(255, 'fa fa-plus'),
(256, 'fa fa-plus-circle'),
(257, 'fa fa-plus-square'),
(258, 'fa fa-plus-square-o'),
(259, 'fa fa-power-off'),
(260, 'fa fa-print'),
(261, 'fa fa-puzzle-piece'),
(262, 'fa fa-qrcode'),
(263, 'fa fa-question'),
(264, 'fa fa-question-circle'),
(265, 'fa fa-quote-left'),
(266, 'fa fa-quote-right'),
(267, 'fa fa-random'),
(268, 'fa fa-recycle'),
(269, 'fa fa-refresh'),
(270, 'fa fa-remove'),
(271, 'fa fa-reorder'),
(272, 'fa fa-reply'),
(273, 'fa fa-reply-all'),
(274, 'fa fa-retweet'),
(275, 'fa fa-road'),
(276, 'fa fa-rocket'),
(277, 'fa fa-rss'),
(278, 'fa fa-rss-square'),
(279, 'fa fa-search'),
(280, 'fa fa-search-minus'),
(281, 'fa fa-search-plus'),
(282, 'fa fa-send'),
(283, 'fa fa-send-o'),
(284, 'fa fa-server'),
(285, 'fa fa-share'),
(286, 'fa fa-share-alt'),
(287, 'fa fa-share-alt-square'),
(288, 'fa fa-share-square'),
(289, 'fa fa-share-square-o'),
(290, 'fa fa-shield'),
(291, 'fa fa-ship'),
(292, 'fa fa-shopping-cart'),
(293, 'fa fa-sign-in'),
(294, 'fa fa-sign-out'),
(295, 'fa fa-signal'),
(296, 'fa fa-sitemap'),
(297, 'fa fa-sliders'),
(298, 'fa fa-smile-o'),
(299, 'fa fa-soccer-ball-o'),
(300, 'fa fa-sort'),
(301, 'fa fa-sort-alpha-asc'),
(302, 'fa fa-sort-alpha-desc'),
(303, 'fa fa-sort-amount-asc'),
(304, 'fa fa-sort-amount-desc'),
(305, 'fa fa-sort-asc'),
(306, 'fa fa-sort-desc'),
(307, 'fa fa-sort-down'),
(308, 'fa fa-sort-numeric-asc'),
(309, 'fa fa-sort-numeric-desc'),
(310, 'fa fa-sort-up'),
(311, 'fa fa-space-shuttle'),
(312, 'fa fa-spinner'),
(313, 'fa fa-spoon'),
(314, 'fa fa-square'),
(315, 'fa fa-square-o'),
(316, 'fa fa-star'),
(317, 'fa fa-star-half'),
(318, 'fa fa-star-half-empty'),
(319, 'fa fa-star-half-full'),
(320, 'fa fa-star-half-o'),
(321, 'fa fa-star-o'),
(322, 'fa fa-street-view'),
(323, 'fa fa-suitcase'),
(324, 'fa fa-sun-o'),
(325, 'fa fa-support'),
(326, 'fa fa-tablet'),
(327, 'fa fa-tachometer'),
(328, 'fa fa-tag'),
(329, 'fa fa-tags'),
(330, 'fa fa-tasks'),
(331, 'fa fa-taxi'),
(332, 'fa fa-terminal'),
(333, 'fa fa-thumb-tack'),
(334, 'fa fa-thumbs-down'),
(335, 'fa fa-thumbs-o-down'),
(336, 'fa fa-thumbs-o-up'),
(337, 'fa fa-thumbs-up'),
(338, 'fa fa-ticket'),
(339, 'fa fa-times'),
(340, 'fa fa-times-circle'),
(341, 'fa fa-times-circle-o'),
(342, 'fa fa-tint'),
(343, 'fa fa-toggle-down'),
(344, 'fa fa-toggle-left'),
(345, 'fa fa-toggle-off'),
(346, 'fa fa-toggle-on'),
(347, 'fa fa-toggle-right'),
(348, 'fa fa-toggle-up'),
(349, 'fa fa-trash'),
(350, 'fa fa-trash-o'),
(351, 'fa fa-tree'),
(352, 'fa fa-trophy'),
(353, 'fa fa-truck'),
(354, 'fa fa-tty'),
(355, 'fa fa-umbrella'),
(356, 'fa fa-university'),
(357, 'fa fa-unlock'),
(358, 'fa fa-unlock-alt'),
(359, 'fa fa-unsorted'),
(360, 'fa fa-upload'),
(361, 'fa fa-user'),
(362, 'fa fa-user-plus'),
(363, 'fa fa-user-secret'),
(364, 'fa fa-user-times'),
(365, 'fa fa-users'),
(366, 'fa fa-video-camera'),
(367, 'fa fa-volume-down'),
(368, 'fa fa-volume-off'),
(369, 'fa fa-volume-up'),
(370, 'fa fa-warning'),
(371, 'fa fa-wheelchair'),
(372, 'fa fa-wifi'),
(373, 'fa fa-wrench'),
(374, 'fa fa-ambulance'),
(375, 'fa fa-automobile'),
(376, 'fa fa-bicycle'),
(377, 'fa fa-bus'),
(378, 'fa fa-cab'),
(379, 'fa fa-car'),
(380, 'fa fa-fighter-jet'),
(381, 'fa fa-motorcycle'),
(382, 'fa fa-plane'),
(383, 'fa fa-rocket'),
(384, 'fa fa-ship'),
(385, 'fa fa-space-shuttle'),
(386, 'fa fa-subway'),
(387, 'fa fa-taxi'),
(388, 'fa fa-train'),
(389, 'fa fa-truck'),
(390, 'fa fa-wheelchair'),
(391, 'fa fa-circle-thin'),
(392, 'fa fa-genderless'),
(393, 'fa fa-mars'),
(394, 'fa fa-mars-double'),
(395, 'fa fa-mars-stroke'),
(396, 'fa fa-mars-stroke-h'),
(397, 'fa fa-mars-stroke-v'),
(398, 'fa fa-mercury'),
(399, 'fa fa-neuter'),
(400, 'fa fa-transgender'),
(401, 'fa fa-transgender-alt'),
(402, 'fa fa-venus'),
(403, 'fa fa-venus-double'),
(404, 'fa fa-venus-mars'),
(405, 'fa fa-file'),
(406, 'fa fa-file-archive-o'),
(407, 'fa fa-file-audio-o'),
(408, 'fa fa-file-code-o'),
(409, 'fa fa-file-excel-o'),
(410, 'fa fa-file-image-o'),
(411, 'fa fa-file-movie-o'),
(412, 'fa fa-file-o'),
(413, 'fa fa-file-pdf-o'),
(414, 'fa fa-file-photo-o'),
(415, 'fa fa-file-picture-o'),
(416, 'fa fa-file-powerpoint-o'),
(417, 'fa fa-file-sound-o'),
(418, 'fa fa-file-text'),
(419, 'fa fa-file-text-o'),
(420, 'fa fa-file-video-o'),
(421, 'fa fa-file-word-o'),
(422, 'fa fa-file-zip-o'),
(423, 'fa fa-cog fa-spin'),
(424, 'fa fa-gear fa-spin'),
(425, 'fa fa-refresh fa-spin'),
(426, 'fa fa-spinner fa-spin'),
(427, 'fa fa-check-square'),
(428, 'fa fa-check-square-o'),
(429, 'fa fa-circle'),
(430, 'fa fa-circle-o'),
(431, 'fa fafa fa-dot-circle-o'),
(432, 'fa fa-minus-square'),
(433, 'fa fa-minus-square-o'),
(434, 'fa fa-plus-square'),
(435, 'fa fa-plus-square-o'),
(436, 'fa fa-square'),
(437, 'fa fa-square-o'),
(438, 'fa fa-cc-amex'),
(439, 'fa fa-cc-discover'),
(440, 'fa fa-cc-mastercard'),
(441, 'fa fa-cc-paypal'),
(442, 'fa fa-cc-stripe'),
(443, 'fa fa-cc-visa'),
(444, 'fa fa-credit-card'),
(445, 'fa fa-google-wallet'),
(446, 'fa fa-paypal'),
(447, 'fa fa-area-chart'),
(448, 'fa fa-bar-chart'),
(449, 'fa fa-bar-chart-o'),
(450, 'fa fa-line-chart'),
(451, 'fa fa-pie-chart'),
(452, 'fa fa-bitcoin'),
(453, 'fa fa-btc'),
(454, 'fa fa-cny'),
(455, 'fa fa-dollar'),
(456, 'fa fa-eur'),
(457, 'fa fa-euro'),
(458, 'fa fa-gbp'),
(459, 'fa fa-ils'),
(460, 'fa fa-inr'),
(461, 'fa fa-jpy'),
(462, 'fa fa-krw'),
(463, 'fa fa-money'),
(464, 'fa fa-rmb'),
(465, 'fa fa-rouble'),
(466, 'fa fa-rub'),
(467, 'fa fa-ruble'),
(468, 'fa fa-rupee'),
(469, 'fa fa-shekel'),
(470, 'fa fa-sheqel'),
(471, 'fa fa-try'),
(472, 'fa fa-turkish-lira'),
(473, 'fa fa-usd'),
(474, 'fa fa-won'),
(475, 'fa fa-yen'),
(476, 'fa fa-align-center'),
(477, 'fa fa-align-justify'),
(478, 'fa fa-align-left'),
(479, 'fa fa-align-right'),
(480, 'fa fa-bold'),
(481, 'fa fa-chain'),
(482, 'fa fa-chain-broken'),
(483, 'fa fa-clipboard'),
(484, 'fa fa-columns'),
(485, 'fa fa-copy'),
(486, 'fa fa-cut'),
(487, 'fa fa-dedent'),
(488, 'fa fa-eraser'),
(489, 'fa fa-file'),
(490, 'fa fa-file-o'),
(491, 'fa fa-file-text'),
(492, 'fa fa-file-text-o'),
(493, 'fa fa-files-o'),
(494, 'fa fa-floppy-o'),
(495, 'fa fa-font'),
(496, 'fa fa-header'),
(497, 'fa fa-indent'),
(498, 'fa fa-italic'),
(499, 'fa fa-link'),
(500, 'fa fa-list'),
(501, 'fa fa-list-alt'),
(502, 'fa fa-list-ol'),
(503, 'fa fa-list-ul'),
(504, 'fa fa-outdent'),
(505, 'fa fa-paperclip'),
(506, 'fa fa-paragraph'),
(507, 'fa fa-paste'),
(508, 'fa fa-repeat'),
(509, 'fa fa-rotate-left'),
(510, 'fa fa-rotate-right'),
(511, 'fa fa-save'),
(512, 'fa fa-scissors'),
(513, 'fa fa-strikethrough'),
(514, 'fa fa-subscript'),
(515, 'fa fa-superscript'),
(516, 'fa fa-table'),
(517, 'fa fa-text-height'),
(518, 'fa fa-text-width'),
(519, 'fa fa-th'),
(520, 'fa fa-th-large'),
(521, 'fa fa-th-list'),
(522, 'fa fa-underline'),
(523, 'fa fa-undo'),
(524, 'fa fa-unlink'),
(525, 'fa fa-angle-double-down'),
(526, 'fa fa-angle-double-left'),
(527, 'fa fa-angle-double-right'),
(528, 'fa fa-angle-double-up'),
(529, 'fa fa-angle-down'),
(530, 'fa fa-angle-left'),
(531, 'fa fa-angle-right'),
(532, 'fa fa-angle-up'),
(533, 'fa fa-arrow-circle-down'),
(534, 'fa fa-arrow-circle-left'),
(535, 'fa fa-arrow-circle-o-down'),
(536, 'fa fa-arrow-circle-o-left'),
(537, 'fa fa-arrow-circle-o-right'),
(538, 'fa fa-arrow-circle-o-up'),
(539, 'fa fa-arrow-circle-right'),
(540, 'fa fa-arrow-circle-up'),
(541, 'fa fa-arrow-down'),
(542, 'fa fa-arrow-left'),
(543, 'fa fa-arrow-right'),
(544, 'fa fa-arrow-up'),
(545, 'fa fa-arrows'),
(546, 'fa fa-arrows-alt'),
(547, 'fa fa-arrows-h'),
(548, 'fa fa-arrows-v'),
(549, 'fa fa-caret-down'),
(550, 'fa fa-caret-left'),
(551, 'fa fa-caret-right'),
(552, 'fa fa-caret-square-o-down'),
(553, 'fa fa-caret-square-o-left'),
(554, 'fa fa-caret-square-o-right'),
(555, 'fa fa-caret-square-o-up'),
(556, 'fa fa-caret-up'),
(557, 'fa fa-chevron-circle-down'),
(558, 'fa fa-chevron-circle-left'),
(559, 'fa fa-chevron-circle-right'),
(560, 'fa fa-chevron-circle-up'),
(561, 'fa fa-chevron-down'),
(562, 'fa fa-chevron-left'),
(563, 'fa fa-chevron-right'),
(564, 'fa fa-chevron-up'),
(565, 'fa fa-hand-o-down'),
(566, 'fa fa-hand-o-left'),
(567, 'fa fa-hand-o-right'),
(568, 'fa fa-hand-o-up'),
(569, 'fa fa-long-arrow-down'),
(570, 'fa fa-long-arrow-left'),
(571, 'fa fa-long-arrow-right'),
(572, 'fa fa-long-arrow-up'),
(573, 'fa fa-toggle-down'),
(574, 'fa fa-toggle-left'),
(575, 'fa fa-toggle-right'),
(576, 'fa fa-toggle-up'),
(577, 'fa fa-arrows-alt'),
(578, 'fa fa-backward'),
(579, 'fa fa-compress'),
(580, 'fa fa-eject'),
(581, 'fa fa-expand'),
(582, 'fa fa-fast-backward'),
(583, 'fa fa-fast-forward'),
(584, 'fa fa-forward'),
(585, 'fa fa-pause'),
(586, 'fa fa-play'),
(587, 'fa fa-play-circle'),
(588, 'fa fa-play-circle-o'),
(589, 'fa fa-step-backward'),
(590, 'fa fa-step-forward'),
(591, 'fa fa-stop'),
(592, 'fa fa-youtube-play'),
(593, 'fa fa-adn'),
(594, 'fa fa-android'),
(595, 'fa fa-angellist'),
(596, 'fa fa-apple'),
(597, 'fa fa-behance'),
(598, 'fa fa-behance-square'),
(599, 'fa fa-bitbucket'),
(600, 'fa fa-bitbucket-square'),
(601, 'fa fa-bitcoin'),
(602, 'fa fa-btc'),
(603, 'fa fa-buysellads'),
(604, 'fa fa-cc-amex'),
(605, 'fa fa-cc-discover'),
(606, 'fa fa-cc-mastercard'),
(607, 'fa fa-cc-paypal'),
(608, 'fa fa-cc-stripe'),
(609, 'fa fa-cc-visa'),
(610, 'fa fa-codepen'),
(611, 'fa fa-connectdevelop'),
(612, 'fa fa-css3'),
(613, 'fa fa-dashcube'),
(614, 'fa fa-delicious'),
(615, 'fa fa-deviantart'),
(616, 'fa fa-digg'),
(617, 'fa fa-dribbble'),
(618, 'fa fa-dropbox'),
(619, 'fa fa-drupal'),
(620, 'fa fa-empire'),
(621, 'fa fa-facebook'),
(622, 'fa fa-facebook-f'),
(623, 'fa fa-facebook-square'),
(624, 'fa fa-flickr'),
(625, 'fa fa-forumbee'),
(626, 'fa fa-foursquare'),
(627, 'fa fa-ge'),
(628, 'fa fa-git'),
(629, 'fa fa-git-square'),
(630, 'fa fa-github'),
(631, 'fa fa-github-alt'),
(632, 'fa fa-github-square'),
(633, 'fa fa-gittip'),
(634, 'fa fa-google'),
(635, 'fa fa-google-plus'),
(636, 'fa fa-google-plus-square'),
(637, 'fa fa-google-wallet'),
(638, 'fa fa-gratipay'),
(639, 'fa fa-hacker-news'),
(640, 'fa fa-html5'),
(641, 'fa fa-instagram'),
(642, 'fa fa-ioxhost'),
(643, 'fa fa-joomla'),
(644, 'fa fa-jsfiddle'),
(645, 'fa fa-lastfm'),
(646, 'fa fa-lastfm-square'),
(647, 'fa fa-leanpub'),
(648, 'fa fa-linkedin'),
(649, 'fa fa-linkedin-square'),
(650, 'fa fa-linux'),
(651, 'fa fa-maxcdn'),
(652, 'fa fa-meanpath'),
(653, 'fa fa-medium'),
(654, 'fa fa-openid'),
(655, 'fa fa-pagelines'),
(656, 'fa fa-paypal'),
(657, 'fa fa-pied-piper'),
(658, 'fa fa-pied-piper-alt'),
(659, 'fa fa-pinterest'),
(660, 'fa fa-pinterest-p'),
(661, 'fa fa-pinterest-square'),
(662, 'fa fa-qq'),
(663, 'fa fa-ra'),
(664, 'fa fa-rebel'),
(665, 'fa fa-reddit'),
(666, 'fa fa-reddit-square'),
(667, 'fa fa-renren'),
(668, 'fa fa-sellsy'),
(669, 'fa fa-share-alt'),
(670, 'fa fa-share-alt-square'),
(671, 'fa fa-shirtsinbulk'),
(672, 'fa fa-simplybuilt'),
(673, 'fa fa-skyatlas'),
(674, 'fa fa-skype'),
(675, 'fa fa-slack'),
(676, 'fa fa-slideshare'),
(677, 'fa fa-soundcloud'),
(678, 'fa fa-spotify'),
(679, 'fa fa-stack-exchange'),
(680, 'fa fa-stack-overflow'),
(681, 'fa fa-steam'),
(682, 'fa fa-steam-square'),
(683, 'fa fa-stumbleupon'),
(684, 'fa fa-stumbleupon-circle'),
(685, 'fa fa-tencent-weibo'),
(686, 'fa fa-trello'),
(687, 'fa fa-tumblr'),
(688, 'fa fa-tumblr-square'),
(689, 'fa fa-twitch'),
(690, 'fa fa-twitter'),
(691, 'fa fa-twitter-square'),
(692, 'fa fa-viacoin'),
(693, 'fa fa-vimeo-square'),
(694, 'fa fa-vine'),
(695, 'fa fa-vk'),
(696, 'fa fa-wechat'),
(697, 'fa fa-weibo'),
(698, 'fa fa-weixin'),
(699, 'fa fa-whatsapp'),
(700, 'fa fa-windows'),
(701, 'fa fa-wordpress'),
(702, 'fa fa-xing'),
(703, 'fa fa-xing-square'),
(704, 'fa fa-yahoo'),
(705, 'fa fa-yelp'),
(706, 'fa fa-youtube'),
(707, 'fa fa-youtube-square'),
(708, 'fa fa-ambulance'),
(709, 'fa fa-h-square'),
(710, 'fa fa-heart'),
(711, 'fa fa-heart-o'),
(712, 'fa fa-heartbeat'),
(713, 'fa fa-hospital-o'),
(714, 'fa fa-medkit'),
(715, 'fa fa-plus-square'),
(716, 'fa fa-stethoscope'),
(717, 'fa fa-user-md'),
(718, 'fa fa-wheelchair');

-- --------------------------------------------------------

--
-- Estrutura da tabela `icons_social`
--

CREATE TABLE `icons_social` (
  `icon_id` int(11) NOT NULL,
  `icon_class` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `icons_social`
--

INSERT INTO `icons_social` (`icon_id`, `icon_class`) VALUES
(1, 'fa fa-500px'),
(2, 'fa fa-amazon'),
(3, 'fa fa-adn'),
(4, 'fa fa-android'),
(5, 'fa fa-angellist'),
(6, 'fa fa-apple'),
(7, 'fa fa-bandcamp'),
(8, 'fa fa-behance'),
(9, 'fa fa-behance-square'),
(10, 'fa fa-bitbucket'),
(11, 'fa fa-bitbucket-square'),
(12, 'fa fa-bitcoin'),
(13, 'fa fa-black-tie'),
(14, 'fa fa-bluetooth'),
(15, 'fa fa-bluetooth-b'),
(16, 'fa fa-btc'),
(17, 'fa fa-buysellads'),
(18, 'fa fa-cc-amex'),
(19, 'fa fa-cc-diners-club'),
(20, 'fa fa-cc-mastercard'),
(21, 'fa fa-cc-paypal'),
(22, 'fa fa-cc-stripe'),
(23, 'fa fa-cc-visa'),
(24, 'fa fa-chrome'),
(25, 'fa fa-codepen'),
(26, 'fa fa-codiepie'),
(27, 'fa fa-connectdevelop'),
(28, 'fa fa-contao'),
(29, 'fa fa-css3'),
(30, 'fa fa-dashcube'),
(31, 'fa fa-delicious'),
(32, 'fa fa-deviantart'),
(33, 'fa fa-digg'),
(34, 'fa fa-dribbble'),
(35, 'fa fa-dropbox'),
(36, 'fa fa-drupal'),
(37, 'fa fa-edge'),
(38, 'fa fa-eercast'),
(39, 'fa fa-empire'),
(40, 'fa fa-envira'),
(41, 'fa fa-etsy'),
(42, 'fa fa-expeditedssl'),
(43, 'fa fa-fa'),
(44, 'fa fa-facebook'),
(45, 'fa fa-facebook-f'),
(46, 'fa fa-facebook-official'),
(47, 'fa fa-facebook-square'),
(48, 'fa fa-firefox'),
(49, 'fa fa-first-order'),
(50, 'fa fa-flickr'),
(51, 'fa fa-fonticons'),
(52, 'fa fa-font-awesome'),
(53, 'fa fa-fort-awesome'),
(54, 'fa fa-forumbee'),
(55, 'fa fa-foursquare'),
(56, 'fa fa-free-code-camp'),
(57, 'fa fa-ge'),
(58, 'fa fa-get-pocket'),
(59, 'fa fa-gg'),
(60, 'fa fa-gg-circle'),
(61, 'fa fa-git'),
(62, 'fa fa-git-square'),
(63, 'fa fa-github'),
(64, 'fa fa-github-alt'),
(65, 'fa fa-github-square'),
(66, 'fa fa-gitlab'),
(67, 'fa fa-gittip'),
(68, 'fa fa-glide'),
(69, 'fa fa-glide-g'),
(70, 'fa fa-google'),
(71, 'fa fa-google-plus'),
(72, 'fa fa-google-plus-circle'),
(73, 'fa fa-google-plus-official'),
(74, 'fa fa-google-plus-square'),
(75, 'fa fa-google-wallet'),
(76, 'fa fa-gratipay'),
(77, 'fa fa-grav'),
(78, 'fa fa-hacker-news'),
(79, 'fa fa-houzz'),
(80, 'fa fa-html5'),
(81, 'fa fa-imdb'),
(82, 'fa fa-instagram'),
(83, 'fa fa-internet-explorer'),
(84, 'fa fa-ioxhost'),
(85, 'fa fa-joomla'),
(86, 'fa fa-jsfiddle'),
(87, 'fa fa-lastfm'),
(88, 'fa fa-lastfm-square'),
(89, 'fa fa-leanpub'),
(90, 'fa fa-linkedin'),
(91, 'fa fa-linkedin-square'),
(92, 'fa fa-linode'),
(93, 'fa fa-linux'),
(94, 'fa fa-maxcdn'),
(95, 'fa fa-meanpath'),
(96, 'fa fa-medium'),
(97, 'fa fa-meetup'),
(98, 'fa fa-mixcloud'),
(99, 'fa fa-modx'),
(100, 'fa fa-odnoklassniki'),
(101, 'fa fa-odnoklassniki-square'),
(102, 'fa fa-opencart'),
(103, 'fa fa-openid'),
(104, 'fa fa-opera'),
(105, 'fa fa-optin-monster'),
(106, 'fa fa-pagelines'),
(107, 'fa fa-paypal'),
(108, 'fa fa-pied-piper'),
(109, 'fa fa-pied-piper-alt'),
(110, 'fa fa-pinterest'),
(111, 'fa fa-pinterest-p'),
(112, 'fa fa-pinterest-square'),
(113, 'fa fa-product-hunt'),
(114, 'fa fa-qq'),
(115, 'fa fa-quora'),
(116, 'fa fa-ra'),
(117, 'fa fa-ravelry'),
(118, 'fa fa-rebel'),
(119, 'fa fa-reddit'),
(120, 'fa fa-reddit-alien'),
(121, 'fa fa-reddit-square'),
(122, 'fa fa-renren'),
(123, 'fa fa-safari'),
(124, 'fa fa-scribd'),
(125, 'fa fa-sellsy'),
(126, 'fa fa-share-alt'),
(127, 'fa fa-share-alt-square'),
(128, 'fa fa-shirtsinbulk'),
(129, 'fa fa-snapchat'),
(130, 'fa fa-snapchat-square'),
(131, 'fa fa-simplybuilt'),
(132, 'fa fa-skyatlas'),
(133, 'fa fa-skype'),
(134, 'fa fa-slack'),
(135, 'fa fa-slideshare'),
(136, 'fa fa-soundcloud'),
(137, 'fa fa-spotify'),
(138, 'fa fa-stack-exchange'),
(139, 'fa fa-stack-overflow'),
(140, 'fa fa-steam'),
(141, 'fa fa-steam-square'),
(142, 'fa fa-stumbleupon'),
(143, 'fa fa-stumbleupon-circle'),
(144, 'fa fa-superpowers'),
(145, 'fa fa-telegram'),
(146, 'fa fa-tencent-weibo'),
(147, 'fa fa-themeisle'),
(148, 'fa fa-trello'),
(149, 'fa fa-tripadvisor'),
(150, 'fa fa-tumblr'),
(151, 'fa fa-tumblr-square'),
(152, 'fa fa-twitch'),
(153, 'fa fa-twitter'),
(154, 'fa fa-twitter-square'),
(155, 'fa fa-usb'),
(156, 'fa fa-viacoin'),
(157, 'fa fa-viadeo'),
(158, 'fa fa-viadeo-square'),
(159, 'fa fa-vimeo'),
(160, 'fa fa-vimeo-square'),
(161, 'fa fa-vine'),
(162, 'fa fa-vk'),
(163, 'fa fa-wechat'),
(164, 'fa fa-weibo'),
(165, 'fa fa-weixin'),
(166, 'fa fa-whatsapp'),
(167, 'fa fa-wikipedia-w'),
(168, 'fa fa-windows'),
(169, 'fa fa-wordpress'),
(170, 'fa fa-wpbeginner'),
(171, 'fa fa-wpexplorer'),
(172, 'fa fa-wpforms'),
(173, 'fa fa-xing'),
(174, 'fa fa-xing-square'),
(175, 'fa fa-y-combinator'),
(176, 'fa fa-yahoo'),
(177, 'fa fa-yelp'),
(178, 'fa fa-yc'),
(179, 'fa fa-yoast'),
(180, 'fa fa-youtube'),
(181, 'fa fa-youtube-play'),
(182, 'fa fa-youtube-square');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

CREATE TABLE `mensagens` (
  `mensagem_id` int(10) NOT NULL,
  `mensagem_nome` varchar(255) NOT NULL,
  `mensagem_texto` longtext NOT NULL,
  `mensagem_data` date NOT NULL,
  `mensagem_envio` datetime DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mensagens`
--

INSERT INTO `mensagens` (`mensagem_id`, `mensagem_nome`, `mensagem_texto`, `mensagem_data`, `mensagem_envio`) VALUES
(1, 'Mensagem Teste', '<p>teste</p>', '2017-11-11', '2017-11-11 19:16:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo1`
--

CREATE TABLE `modulo1` (
  `modulo1_id` int(11) NOT NULL,
  `modulo1_nome` varchar(200) DEFAULT NULL,
  `modulo1_subtitulo1` text DEFAULT NULL,
  `modulo1_conteudo1` longtext DEFAULT NULL,
  `modulo1_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo1`
--

INSERT INTO `modulo1` (`modulo1_id`, `modulo1_nome`, `modulo1_subtitulo1`, `modulo1_conteudo1`, `modulo1_status`) VALUES
(1, 'Bem-Vindo(a)!!!!', 'Seja bem-vindo ao nosso Blog', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>ol&aacute;, teste bem vindo a nossa empresa</p>\r\n<p>&nbsp;</p>\r\n<table style=\"height: 569px;\" width=\"1115\">\r\n<tbody>\r\n<tr style=\"height: 706.4px;\">\r\n<td style=\"width: 514.4px; height: 706.4px;\"><img src=\"../../images/71507657484.jpg\" width=\"500\" height=\"342\" /></td>\r\n<td style=\"width: 634.4px; height: 706.4px;\"><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAEsAfQDASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAwIEBQcBBggACf/EAFYQAAEDAgQDBQUFBAUIBA0FAAECAxEABAUSITEGB0ETIlFhcQgUMoGRI0KhscEVUtHwFyQzYuEWQ3KSo7Kz8TRVY4IJGCUnNUVHU2Vzg6LCJihUddL/xAAbAQACAwEBAQAAAAAAAAAAAAAAAgEDBAUGB//EAEARAAIBAwIDBAQLBwQDAQAAAAABAgMEERIhBTFBBhNRYSIyccEHFDNCYnKBkaGx8BUjUrLR0uEkY8LxU3OCov/aAAwDAQACEQMRAD8A+g/ZJP3N6R2EbinpaM6bUkoOoIGlcjuzoDJbCSIImhKtRBlMeFPykHpSFJgwahwwBFO2SYkifUVUnMVGVlUjZ3SrsU2kzVM8zUw26I2dFXWvrNB1RWvjXqwpQTv1pOdWbKQK2s0C69XqwpWUTUAZrIMb7UPtRInw1r3aCdflSyQ0XhjgEHrNKzGZmgZgn70UoLIpC4cBQI1NKCjIM03CwaUFHoTpQ1kBylYmnCViJBApgF+OlFSuRrtVTg0RjBItqk70RKxM7UwbdI1zGDRO3EzrSDJ5Hodg6kGl9uoR08KZJdKhIpQcJ3OtK4lkZNciRTcaACiB5MCd6je0yxMCdKWHSJFI14lqqJ7Eql8abzRFPQdDvUYh4mDJmiqeOmU/WqpRXIbkx+XQYA61A3vDnvDq3ijVZmaf9sQQQT51urGFJcs23MupTJgVQk4MqrSeEVM/wuZPcO1MXuHSjTs5q3X8GSArumT5VH3GDIynudPCrNaKNWCpl4GsHVB18qbOYOQJyERtptVoXGDBJMpgeNRzuEJSDCN/KmH1FbLwtaSCEbGmzmHr70jx3qwXsHSBqnbyqPfwkQTlj5Uk5YWw0ZI0R2zWgajT8qZu2xkjJoda3e4woQTlOniKi7jDBmJg/KqC9PJqS7aCTuabLYiCBG9bS7h+U6ppi7YAEnXTagk1xaNx+FN3GR0n6VNvWBCoAim7lqBrpVtOXQrmQ5bIE9KmcJblkaH+d6bG10OgInwqdwWzzMgkbaxFbqD9IxTQZlhRTIGtcyc4bQq4wuUEDWJ8OtdaW9jKNEzHSK5j5xWoHGlyInQa0tzU3Rps46pNGu8NcPsfs1q+WgFSVkCa+yXILDsOtOT/AAl7nZtMheD2q1BI3UWklRnzM18jOHEgcMtGN3D09K+t/IHEG3eT3CU7owm2R9GwP0peEy11qj/XMnjkVClSx1LGCUgQBFZgUL3hMgeO1Z7ZI0Jg13cM85kUpsK6xXEntXsJb9orhjvElzDmD6favCu2g4D5VxB7WT3a+05wa0hUpGGshQ6T2r3+Fc7iibopeaOhw2WKsn9Flg4Db5X1Qfu1NZVfummeEtBLpUdimpQAGTXNaUjepYGqkT0g0MgjQinToEBXyoYQVJzaQKqUPSwWDUo1miBMbJomUTMVmrIwSAbLAG1Ao6zBJNBVBOlQqaBPHIAokqM0F3XpRlRmMUFe+tRKn4FkZZG7gG9NnEkGfGnToj5mgOEBMeNVYfMcDXq9A/k16rO6ZGToCvV6vV0DhmFICt96CpMGDR6wpIUPOgBspA6VS/NAFLL8mYeEVdZESKpfmmkhm4Pg6D+FXW6WsOqKsUTO0R96g0RbgUIH40OtUl1L0ZBI266VjyrwINYKsukelKSZr00gKVBOlYGqtSagAuwInelBxUyTIochIjU1nTxpGh4yxzDhYJrylhMTQQY66DWkqcE6mTUFiaY5SobpNEDnTYGmYX0Bilh2ExuagkfoWdp1rPaf3qaocMZh18az2h30pXBMjA67XXeiJdIGhimfaDzrIXmTvSuHgMngeh/xVNKD2g0mmIc8/Klh31+VI10ZKZJIdmD+NELo0iR+VRqXj1VAoinjEEzVLjjmaovKyyRSsaSRvVyWNsleHW6v+zTqPSqKQ5K0EqO4FdBYY2k4ZbR1aSfwqproZbh4wiMfs0SRGm9MX7Ibgb6RFbE60noOlMnmR4VVOHVGdSaNZuLJAEH8qj3rAEap+dbS/bgjXT0pg9bAzIqrU/EdPJqb9h3SSmajnsP0OkittuLYZZH40wuLdOUyY8PWjOSUzTrmxAkJMHpp1qMubHQ9w6aVK8Q8V8O4LbuPXWIMqyIUtSULSTAmZMwNvWqF4/8AaawnCrhTPCz5u3C3PZqt0ZEmD8SyonfwA8iKzuum9NNOT8ve+SOjSt5411Gorz/pzZab1nrsCajH7OFEaEg6jrXI/EHtC8f45eKuFYwuySO6ewcyhKf7oSAfn+OtSHDvPniPBG3PeMWur9HdUn3iFKiehVPSdCSNZ1rbC2rSjmSwymVekniMsnTL1jsSkima7GDtWq8P89uD8fw9i6ffFqpQyvNrEKSqB3hqZTMj86361ctMStkXti+h5lwSlSf50qtpweHsRKSkk0QirJRGg+tbFgth9gFZKF7jJ1A1O1bPgtkfdU6HTTQVspP0jLUPWtgCASmIrlHnmyGeO7lAHQfrXZ9rZ5RGuvjXHXtAdzmHcNQNhP41TcPdGvh282RPDzI/yTSqfvmvqt7Pqf8AzP8ACwH/AFax/uCvljgLWXg9KjP9oa+p/s9z/Q/wtP8A1ax/uCm4DLVWqjdpIpUqOCxhoIr1erIBO1emPImK4v8AaXtQ57TnC7hPwWDUaf33a7V7MDc61x17RrOf2kuGnNdLJkH/AF3K5fF3poL6yOpwqOa0k/4WWNYIyuk9QKeKBB7oMUG1RlWT1jWnVcuHqm5AT4GvAQAPKl5O9JpRA3I2ph3JsarBknLFCUdCKdKg9NKaOJ9dakaLygKviNCUB0NFUDMmhrAgRpQSBcA3Ijzpuv4vlTlwAp6/Km6xrPyoGjzGzpEkbGmzqvug+tOXonamzkeBkUrSawWA69Xq9TAdAV6vV6tRxD1eVoknyr1IcXAyigAZM6mqY5qf2F0f+1H5Vc1UzzTIDV3P/vE1fb+uR1RUtJhWkqiaUTFDVM61rayXpiyQDrvFIUrNWK8SBuarwxjwMGRXiddetJUobCkST6DagAsn5dK9oBQyqRrWCZ3EGgAoXpoawpUamkFRI1pClHprUYAMFA/KlZj00puFjrM0tKtQZmKhxyMpNDpK1RodOleCiUwDpQM+2tezeY+tJhlmpDhTxB3is9oTBG1NisDrNezCN/xoGHXa/wB38aWl0eMTTPOIjMKyFnYEVDWeYD4OdNyN6X2saZhTEKUSBr50YOAAFRk1nnHoPGbXIdId2NdI4WB+yrVXUtp/KuZUXKAtKdd66bwsThNtqP7NP5VQ1h7iVnnBlwTPkabuJB6b07WIMxTdzY6UjM5Hut+OhFM3Wv58KkXknXzpm6DBPjVVSKSyiYvDIbEX7XDrZ69vnUMsMoK3HFqhKUjck1x9zj9o7FrvFLjDeFL7/wAmtqyo93Sn7YayVKM6R4abetPva35vYoviFPL/AAy9dbw1toe8hACA69JkEnVSUxpsJneAa5lsnOI8UxEWeHsXDycxIR2hyrHmEmOn4/VVSTjqnyNtJODxFZl+QjizjbjLiG5cTir7uQCFNqcKwP0HyrUX13ORSNSFfFGk+U9fSrsseUnEj1t7zi2HFsEFXZ5MiR4AqOqvCBptWicU8GYhh9yG3mezJJlJSQBEaegBq62uqDeiGPsLbizrwWupn7TQG7V15SlI+FPXfXypSLVxKVPvqKUJICdNz+tbA7h1ww57uhuFhJUoFOqUjfT6iPI+FZfatVdk273yGitwq1SVkbDyAFdKnNt7HMnSXXmQbD2IW7zZw8lKVJMxqCkiYJ8x0ro72d+ZjV3ibnB2M3IZdfSk2mYylagNUjwPSOp9Yqgiyp5Di2EgFai6nTSQo6Dy6U+w5L9vijGNWLjja0BL6Ft6KSUqHXoRMz5VNaiq0cNFcJum8J7HfItSFHujStowK2z2gzJ1Fahwhix4h4bw/GFhJcuGElzKIGcCFR5SDVi4HbxZoOT6Vy6ScZOLNMnncKzbKiY1ri72iLYNcxbjXUpH613KhmE6j6VxN7SbATzCuFE/dT+tV3Lw4+03cOWakvYQuBsuq4LSkCTnJ36V9SfZ/Tl5RcLif/Vlv/uCvl/w8FI4JQoDUrIn1r6ichwRyn4ZB/6tt/8AcFP2cfp1vaN2m2pUUb/RWvhPrQwCdhNLbzAkEaV6g8jHmErj/wBoWE+0Zw3Oua0aEf8AecrsA1x/7QqkH2j+GUmCfdGo8+85XH4033EV9KJ1uFLNaX1ZFlNpGYqolIb60sCflWCCeMM1cj1YUQAdazSVmNKfAZB0J1OszvRaC5OYzUEp4ArSNulN3BGhpw4Ugz1pus6aa0FieQZMCTTZySSoHrTkmBTZauhBk60EjN34iOlCWlRBhXyorwOYmNDTdahMA0FkXsJIIMGvUkrAMa16gjUX72p/drId3kUEKBE7VmQNzWo44QumNqRvSM/8JpJJOhM1DeAF5tYGtUxzW/6PeHqHE1cgHWqc5rCbe81j7RP5Vptt5iyeMe0qNajMCkkxua9QyqTrtWzBcmZUddyNKTJO9erEjxqBjNJUqI0rBXB33pG2wFI44JTFleuleUqdjtSCQNyBSe0TAPjSkhCok0Na8unj0pKnf3dKGtZ3NABUujYiBREq6jamoWDXgoGgB5n8qznEedNw7pqNax2i/GgBwVDSD61kKkE0BC5He+tKK0iNd6hrJKk0EK9dBpS0q603DgJI8PGlZ8vkD16UriOp+I6QvWlFeYU1DnU6+FKzgiaqlHJYHSYcQfOup8JGbCLXXZpJ/CuUkr76SPHUV1VhJBwez/8AlIP4VmmsNCTk2HXPyps5rMmjqMk0FW5qqSK2NXelM3AcsgdDTx3pTRY0mqprKI6nzv5tcJY5i/OXHLItKU4blx1YVJytkZ4nwjT8qvHkdyawnB2v8oMTsWu3WhKkoLYgVYXGXLAP8dXPFzTaHUYnbtMuEDvMutlIB8wpGYeRAraXRgfDuFJ/aGJW1k0gRndcCBp61yL+dSWKa5eB6zhDpxg6r9Y1riK1t+8phDQcUMiCUA9mnqR4f4Vzbzww1iwSLlACHGgpREaHz9SavfEeKOG759xWFcR4fdriSlt4EgTvVccy2+CsXwh+24jxmwssyFdm7cXCGyk+WYia51CUqdxFtdTuV6cZ20kmuXU4/wAWxZpZXMFS0L73nr+OpqBXdh5tTSyQsgxHnOn5UfGmrFOJ3DVhdG6S2o5FJ1z/AOjGhH6VJ8FcAY1xpiqLbTDcNSM93iNxqhhsbkJ3UroEjckbb17xThQp66jwkfOlTqXFXuqSy2+hHsELcSR8CFFBKdtSZE9QDr6GpLCGEpU2w9mLIDpBQYzJ+IAfWIqyeLuQrOC4HfYjw/fYmXcLbbceRftoT2rTig2HGigxGYiQdQCdar3C1vM5W+6lxoJQUEbmSJ33rqcPlQvYKrT3RRxK0ueGVe6uFh4z4nYfIC8evuBjb3Ik2l242hY2KVBK/wA1mr3wVs+4p061Rns22vZ8vlNgyBerynfTI3H4VfuCN/1VIiYrhXkVG8qJeI1KTnTi2PG2pHhXEHtKgjmE+lQgFI18N67oQgSPGuHvaVSTzIuJSen61zbyWHH2nX4VvUl7CIwFATwQ1PVzUV9XeUtnh7HLPhhuxJcZ/ZVqUqUIJ+yTM+Gs18p8GbKeDEAER2m30r6xctLRNny94bt0jROF2v8Awk1PZt5lWfmT2oSXcLyfuNhU0gapTB8qwG/GiEeFYg16tM8qsCQgCuOfaH7P/wAZLhfKCSLVrX/vO12SB41x57S7AtvaU4NWB/bYegnz+0eFcri+HRj9ZHR4ZLFWWP4WWQhJB72gokQkg70lCtlUslITArHE0sRSFHqaUZ6Ule9SQIJAMdaCtZUNBtRHfh+dN1npQSBc1iaErQQKK5qIBoKiSIO9RgnLBrgd4n09abuHWiuFR6ECgrjeajBZB5GtxObU6EGmhMamnNxGs600UdCKCxPAMgnXMa9Wa9QKXxXpJ3NJziJ6+FZzDxrRGWo5RmvUkrgwRWFL8KVy3AUpUCT0qneaJC7a93jOKt5a+7EVUHM6PdL3rKk/lWizlmtjyEqLZPzKfUokxSZEwd6zmzUJR18flXSLhSl9APnSdTt6V6dIpC15TETpS4JTFTpNDLhKSDoekUlasxmIoalaaeOlQMLUoqMmsEwJoeYzM1iZpcNMnOUEzCJobizEjxr2m1DcUZAGtS1sCFZx1pQM6igZtYjalJVB3qskMFHYk0oKmB9aBnOxOp2pQWDvpQAbODpWVKgSaCVAdawV6aVOlhkKVER50rNIiTBpuF6SRrSs4IEmoAOlZEEHSl9omJgZvSmwVIgHSlZzHnSSXUsU/EOhxWceZFdY4P8A+iLL/wCQj8q5HQ59okE9RXW2EQMIs4V/mU/lWWst0TIcK0JNBWdCYoqttqAs9KzyK2Ad6U0WdI8acuK7xk6CmqydulVvkR1K+5v8zeHuWOCM4lj96yym5eCAlbgC8kgKcCN1hKlIzRsFTXA/NbiLjPilb3MS+4gXf2hAfYw9aHFtpZUkFJgFKRAIMQRAJJJ0rvHnbyrw/mxw9a4JfOXDYavGitVu6G1lhS0h1MkEfCAdQfh0iZqhOKOVGDcHYhecN2HDjmI4Hist4bbB8pS2nKoFBeUSW1JBjUEFtCAApYUazurSpYk+fXPLB17CjVuYyj83pjOc9Pac38N4PzV4/tbZvCLp+3YeSpZXaKQ2jJJEBLQB0MyFEnxqyuEfZlw+0f8AfuL7x3FromciwUNIGkZwCSo7/ejXUVeHK7BW+XvDVlgN7w4Gr1m1baexGQoPrCRmIEnKCZMU+xK/7MKISlMgkkb1zrzitWTcLf0Y+WzPQ8P4HRSVS5WqXm8r7uRyPzL5cM8M8T+6YOz9lcq+xS2kJiSkEAdRJ38jVtnhhjhTgVlVo1bttXzzLID6MwccSc8OHokpQQTpE9N61PmvdOOcQt3CVglhIIQSe8FGCZ+lXXwEzgGNJtMQx98g2DAW0yogozrSApWQghShGh6SdPB7udarbUk9/eXcPpUba8qygsfroTVrhz+M8PJucdwu3YWvC3bd1honIWwUlogEzGZKiDPQ1xbir1u/xFcC3KkgPZsvihOigT1UT/OtfQG0trO8snnLFopauUKCVKOYqTlgHXy+UCuQmOU2L8S87LfC0Ws2iLld5fkkJCLdK05gY6kwB6+Fe54Naz4Zw5OqsPOX9p4/tNdRvrtaN0lhHRvIvBl4Ry+sS4T/AF1SrtMiCELPc/8Atg/OrnwRH9UTFaxaW7Nrbt27DYQ20kIQkCAABAFbZgQJs066V56db4xcSq+LyYYwVOKiuhIJQN4rhn2lzl5m3CTsAn9a7rgxNcJ+02f/ADm3On3R+tY72O8fadXhb01JewZ4C2lzgxCp+/0+VfWTgYBPBWApHTDbYf7JNfKPhVtC+BgdPjr6t8CGeCMAPjhlt/wk0nZiWZ115lnapfIPyfuJyvV6vV648ierkT2ngFe0bwPpthqD/tX667rkP2mCFe0pwUkiYw1sf7V+uVxf5CP1kdHhfyz+qywEHSPCskwJryU5awswPWsqNTPBSR1NJUqdBSaStYT5mpIPO/D86Av4aWpRUZNDWek0ABc6U3UrXXrRnFddwKbOGPQnWgDCwQnXxoDuunlRnCNNOlN19dKCU8MaXIpo50pzclMb600Og0E0FqZ6vUImTJr1AZL2ziJrwWJ60OvVJzAucExNJUvoKRXqgDJUSINVJzMBFrejTQjaraqpeaAm2vhmjb9a3WKzUfsK6nJe0p2cok0NR6x9KUo6x4UJS4VHQb10C9iioJEmaEtWYydK8pZVoTpQydTGojUUAZUYFIJ0AkaV4melJJSQQelAGCoxp12rAJO0CK8oiZTvSajAZMyQSaGtXQHWlFQFBcWQJioaJTMz56+tZBjUUErEz5RXkrgQVR4aVU1hjKSzgKT1J2oiVT602UskZZnz8aI2qdZ1FC5jB58awSBQyonc1gq6E7n8asECBUmD0rJUBQq8pXVRqMZJQZKuopWfXyoCFdRqDSwonUDbpVbQwtB+0T/pCuucI/8ARVmP+xR+VchoV9ogxrmFddYQqcKtITsykb+VZLjmiUOlk7CgrMn0palDXxoSiACSaxy5gxu4ZBJ603WdYoznSgLOsUkuQvU1rjXHbnAMKXeNYRfXySClQs0Fa0DTXKASZEjaqGsuZlzx9xdb8II4Ou8NQLg9qi6dCl9klJKlr/cUDGm4P1HSzqgQZ61yzx7wxxJwdxtimIWOFW147iK3LxOJXN32RUHVqhpKU6ykaRlI0BJ1Fci8pOUdX6R63s/eNPuIxWcP7f8AosvGOH2LG0CGrp0BKe8l1wr26gmq2xPFc7/uzClFSBOYRuD8JnrGsfTpUhhfEeP2/DjVjxWhlm9CD2amVlxJR0kkAzWp4zi9pYoOIugARlgCST4mPKP+VcmC1y0o9Lr0Ry2Mcc4ZwzGn0F19Ks8grbKpTIPxQCCNJ+tWRw7a4bY8Lt4RaXAW1lCVhJMZgeus1S73F7bzLluhxGdWUoSVjOvfbyHmNYjWK3jl/d3bloF3amwVqzJSjbUbk9T6Cve2s42NopS9Z8jzE18auHp5F14HCbUN5YCUhIA2geFQmCcONYZxJjeMJZSDiHYpCusIzmPqs/KPCprh90OMoQoiRoYo1xcWyb82BuGfeez7Xse0HaBExmy7xOk7TXW4hVlPh3o9cHmriCVwwjesada2nAv+iD1Naw2jatowMRaDzrytH1hZEqEjs5M6+FcJe0yCeZV4rwQn9a7uRmKd48K4V9pYFXMe8kaBI/Wqr940e06fCvlZewDwmgDgdA/eWTX1U5fGeBeH/wD+ttv+GmvlXwgh9fBTZWBlCzH4V9UeXJzcBcPGf/Vlt/w01T2W+VuPb/Ut7UrEKHsf/E2KvV6vV7E8eerkH2kTn9pnhFJOicOb/wB96uvq489olWb2oeFAf+rW/wDfdrl8W+Sj9ZHQ4Y8VpfVZZNDUqTSlmBHjQlKgEA6gVkRrYlxREQRQ1KKjJrxJUZJrGvjUkGCqKGpXU0pZ6T60JzpQANZ0jxpqteb0pws6xTdSCk+VACSepoLh6jqaKpQjT0oDh0j50EjG6PeIpuowJo9xIIA2im6zpHjQWIQTJr1er1BJdUzsaUpzMBJFBBI2NY8qDmBkrKTIM1gqkkz+NDk+NYoAMh2NBVV80NWL7X7o/I1ZwMGaq3mYs+63xidAY+Rrfw/5T7Cur6pTa1QIVOtCpbk6aaUNRiJNby88rw8/wpBgHSslfgKQSZ/negDJI1OlDUSfCvKVpEUkmBNAHpA3NJz6ba1hRk76UFS0kjKqPGgBalgb0F1YJB1ivEk9aGs9JoAUVADMJrwUCNDQ/WvJMGarmJLbcKTpIIpTawkGQdaDJiJ0og9IqILO46m2ginCDoPrWC4oiNKQpWsmk5xG+tWYJyESrLrrP6VlagrahBQnfp+NeKoPpRgMhELynbQ0sugCU7mghUn12rJITuaqkuo6Cocl5s9AoV19hB/8l2pn/Mp/KuPGly63/pDT511/hC5wq1Ef5lH5Vjudmhx2ognSgOKkwOlEWoAETrFAKgNzWKTyxWIXE03JgSaMtXWgq21NVzWUQhu4ARqfSq25wcEWfE9jZ4mMVGGXti6lKLgpzJW2pQzIUJEmArL4E1ZDpEx4Gtb434aTxdw/c4MLxdq44mWn0iezXBAJHUa7dayzjri4m20q9xVjUzjBRvMDEuF+GcHd7G9F1dAAJUojvDYgCdPWqExvjj9sBbS7dLbTWgJTr+Aj8quK+9lriO8Ycv8AibmFZsdiFLMMqU2lI1krUoQN5MVzNxtaWWAcxH+DsNxi3xW1YUG/fmk5AtQAzhMyNCSPUGOlV2fDoQ9J7tHdueLqq9EHsyWt7y+VflNpbpJMjtVbiYTHoJ6efjFXzwa0pixtJJBShO9VxwvwtaWfZuhwuFGgkSBHUa/zNXVwfy/4p4rt0N4NaC3tQYXevyloCdQkxKzvoPmRvS151r6tGlRTbXRG2hGnYUZVa0kk/E2vBuI7e2eRbMIcu7p0wza26c7jivADYddToBJNb7wRypTb4xi/HnF7yXcfxayNk2hCypjDbQHMGkbBSswClL8RAgTLnhDgfhXl3h7rzS1PXhR/WL55IDr390D7iZiE9esnWoLjvj2/vLF/C8GzI7RGQZTqZ0knwG9fR+G2NS3op3bWfBcl/V/geA4pfK+q6LNYj4vr/j8yh8O9sLgtWKu4TjXDOL2DtrcKtbjKpt0MrSopMwQSAQdq6c4SxKwxjBbbFMLu27m0umw4y82qUrSdiDXzW5wcHM8D8Wm4TiKnbvHR+0X7fs47HvqCTM7rIUqOmhkzW08h+fPEPKjGUN29wu+wC6dBv8KWoAawC6wTol0QJGgVAB6Ec58Fp1oOVu8TT5N8/wDJVO7nSnpmso+kKPgFcJ+0qSeZN4nfug/nXbnD2P4TxLgdnxBgl4i6sL5pLzLqfvJPluCDIIOoIINcSe0ignmXfLOwQAK8hxFadKlzyel4N6dV48BXCcf5DNIG4VX1G5ameX/Dsx/6Nt9v/livlnwqpQ4MaUNgs19SeWBKuXnDaj1wu2/4aaz9lPlbj2+9l3av1KHsfuNnr1er1e0PGnq469oLve1FwzEkjDkT9XK7GG9cb8/l/wD7p+G07Th7evoV/wAa5XF/kofWXvOjwxfvJ/VfuLIVJOtCckEnx0oyiDB603WuTJ2FZUad8iaQ4qNJivFZnTahuKmpAVSVgRJ3G1eSsRBpK1D5CgAayIps6YJVIPzoq1BI86bL2oAQTuaC4dz40VRgetAXuNflQSMbk970NBKireiXBhUUBRgb60Fi5CFGTIr1IKlT3UyPWvUElztXLT6A4y6hxJ1zIUCD9KXnMV888D5ycacC3zF7w9jDpQ0qTZurUphwdUlJOg8xBrtHlRzTwbmtwu1j+FfZvIPZXdspQKmHQNUny6g9QauqUZUuZy4vVyN7Cz1r2cxQc5+teCiOtUkhgszrVWczYDF7ofgB/OrOCzOtVjzMKVMXuumUfrW/h/yv2FVbaKKbWRmJnShrMmOlSSsGuRw+OIg40q2979yUmTnSvJnBIiIImNfumout5enk9SFxNKUSBIpvcvpt2HLhz4W0lR9AJoAZYvj+EYG2HMTvm2MwOVKjqqPAVrf9J/DsqhL5SPhMDvelVNjHEV5xJjD+IXzLimlLPYpJEJTOn6UNLgQO8lBTlyiDGX18TWaVV52KnN9C0v6WsHLmT9m3u8SAmPXekN818AccW2La7Tk3zJAFViOzW+Mrqj2p8fhoT6lpcCmnO1CTlkJBmaV1JNYDWy1m+afDzgJyvpAEzlmiHmVw2QVKcfTA1Bb2qnHmlSlKErCCZKJ0HnTYrS8XB2alLQYSEk/TzoU5ZJ1l1jmRwrIi+V3v7hpR5k8JJBz4lEdCk1SK0hOUIbMGYzDWPCOtBcbShDau1OYKJypRPzodSTI15Lqf5qcNM5glVy4U9A0RPzNM1c48ILee1w+4XpMLKUmqeUslf9cWouKHwpTEGdjNDdcdQVOhpBSTAAGon/kaXVINRbDvOZCwVW+EAkCYW9v9BULec68bSooYw23QCCQrVVV88oLQoHuGQTCdqZLDoX2q1nKBIlU5x4CKZOT6kamb3/TNxeoZc1oMxKk/ZCCIrDvOvixlMqYtTlgq+z6fKtF7chaUFpGfbvJ+IelAVcJekAoSoQYSIO3/ADp034hraLl4I50t4vfKwviVpq1WT9lcIkIIOwUDtVpNutPIDrTiVoUJCkmQfnXGt1cZX4za6iBvV1ciOKbu9F1gD7pdaaaD7RUqSNYUPxFXunLGehNKq29LLkZV9u2IPxj8668wpZGF2yZn7JOvyFcgMkh9tU7KH5115hZjC7U/9ij8hXMvFho152HhJPyEUJRJOvSslRJms91Q2rGRzALUPpTZxZGvU04dSelMb+5t7K2cvLx9ti3YQp111xQSlCEiSok6AAAkmq5trkNHAO4ube0ZXcXLzbTSBKlrUEpT6k7VXXMLn3yw5cNRjvEjDtypBcTa2h7ZwpHjl0TPTMRPSuJfaC5+Yzzg4ouLSxvHbfhSwWpuws9Uh6P888Dus7wdEjQayTRGNYuFqSxbmUp07oAzHx0roU+GZSc3gondL5p0jx/7SmLc7uK04MlNxhHA+GD368s2jmevEIUAkPQRIWtTackwMxVrE1V/MrCneGeOGlPKWo+8GXFEKzhaScwUNFAiCFCQRB60jlVwPxXifCuN4zYYJdv2909b2QuktEttuELVlzR0AzGNu7O4m1rPCb/mFi+F8F8xkAX2E3DQs8VLQ95ORKUBh+dHUEAGT3s0me8qezGxToKNJeOSKNRvLlz2LL4UwBvgm2w2+xjC041cthHaWtwsttKWAFKbhJk5QCJJKSd0kaV07gXMLhviHAG7/hsJbaQkNKtUpSlVstIH2ZSNBHSNIgjSqK4kWpJU89coK1K0lUBQAMkkDQRvHQesx3DWMPcLYgvEsAwW7ubV4hFy202t1y4SnqQNBG49T410bW1o2SzSjhstu3UvMd5LOC8MfxJ5eHEuqKlrUANdyY2/X8K1z3dDyH8zSV2dqgu3jp0DzsHJbp8pGZcfdSU/e0J2mJ8cIs7Tgxed28aC/fFIPZWaDoXHJHxCIS2YJVmEd1RSnmRwqvAuUvFOHYTcPqY4f4cxDsnXCc79wWVKeeJ/eHeiOqusGivWwstmenBQWEfO/mpxI5xTzEx3FlXQfS5eOhpwGUlsKhJHkQJ+dQVot5GjaVE+CQdaAUpbWtdxMAnPH4AV7iEYtg16MFuLcsXPYtvFrNHZJcSFozjU5ikpVHSRPhWSOXu3hGSTbeSzOCud3MPga1GHYLxliGHWyFdohlp/OhCpkkIWFN+ZGWD1o+L81r/ju/dxHijELNy8dIQLhtgs5xrBUgApEeKT8qqSyYGYpu0h8r0Ofb6Cn5w/DHkqtsnZKAkKQYUlUTofDyM1qqcNteKQSqxy/Hk8+0mhf3FhPXRePxR0xwRkveDS1ZvtXK2yc4YWHI/1Zr6l8sElPLvhsHf9l23/AA018E8GxriDhjG2/d8SuGHkjMxcsrKFKT5Ea+orpW19uP2h2cPtcDtOZymm7ZhDIZt7CzbUhIAA73ZTPz3NcCh2dhwTXcU6jam8Yaw0937GvM6V/wAanxeNOnKCTinvnZ8j6/Vr/GXEdvw7hofecSlTisqBOpME/kK+U9v7Y/tIMAMJ5pYkVJ7ylrDKwPIko0/nSpGy9sfnLiV82xxvj7HElokT2T1s0y42k79mthKe9A3UlQ10FdFWdatTbpc+hyJ1FB+lyO8MA9o22xTie4wEM50tPpt0EESTsT9TFUpzexp7Gvamwd5bJbQywlhufvJAJn6k1dvIrl3yW4v4TwnmpwjhV12uLtB9S7i7cU40+lRC0qTmy5krChoIMSNIqk+bTaWParw21CdGWUgGP7s/rXluJ0a9JU41nupe5na4S6c3UlBfMf5ot95WkHWaAr4TRXelN1qn0FPEtYmdYpLh0iPOsFRmRWCSTJqSDEjrpSFq3EiKUvahLjKZ2oACSSZNJWQBFZoVACV7U2cO8+lOVgRrTV2dYHjQAweICqCsgT3hBor+hOlN1mEzAMUDxfQEXHOhH0r1YJkzEV6gc4dxjCXMQtk3L92m0hILYQqVEeJq6fYs4kewvmLdcOG4UbfEcMLiwtXxOtKGU/6qlVSj2F4m/ZdkkoWVa5lD4Y6aUxcv77g/E7DE7bElWt1Ck9tbuFJAUIIkbV260NcGjiweHk+qQu2hu4n/AFhXhdtCSXE/6wr5rNcxuNHocZ4ixBQgEj3tZJB+dKc5g8aLSC3xHiiUq6G6c3+utcnuZF2pH0pF00dlpP8A3hVacylJLV2QoH7Mda4bHMvjphIaPFGJEAQYuVkx9ax/SHxY6Mxx3EHwsZcrr6iZ+dabbVQnqZXUSqLB09gTicQwXGcFVCldim/YBVAStmStXmeyLula6VjNvMVUvKvjrF8J5g4I/jzyrrDX7xNrdsOKlLjLoyKB8oVUJxTjHE3CfE+NcM3eMruXsKvXrNbzZIQstrKcyZ6GJHrWjvXhtL9P9MsTS3L1KyetalzKxxWD8NuoYWA9eHsEGdgdz9Kpxzj3iFSy2bx8J3nNqPSnl9fX2IYa2ziNwp9TgLkKVMA7HyodVtYwRKWVhEZaNFCu3eyITlzDUmYnWjocUohBOaBnOnwp/WkptX2wWVFtSUABJKep6D5Udpp4tEuMFtQMZwQmQBJH4VSVrYSFML7Z0kQgBIJQYURt6U3Q321wl/OoII1AgEnrFFQlrLkUhZBGxH3o1MfSsBTjRQlxKFZVCFAbjU6jp0+lBIJJlThNwWm1aJWVb9INAfYIdytqCVpSFKgE5td6Jmt7lxCUtKCgsgpBjMB+VDuULs3ULVmTCz3d4EeVACXgp52O0S0nKEZp1BpsC7bAtB1SUgEq7wOadPXxo6FPZgp1LZC82Vca5juTrJ9KzcIdbSQqyCToG1r3idYB+dAEat+5WewZZVmzaL3lPWfrSV/bJJdQnIju91WpPrTu4RKyph5sZEqlRGs+Z6bim7wcWmWnEkhEKWFzG1SAyWsogOkErVAIMECd6wotoZkPoLkQkDcbyD0rz7D5QEhxJEg91MmPn0+dNHbS4fSkhAUdgkK3O0Uy3AE4MuQhRLikAECmy3FBIQkHUHNOpnWni2XlIyMpbStJhYPxAAbCKj7i3WAoOOQkaHJuf1p0JJEUq4aS64yt4qA1Mdas7kHi2HYVj2Ju4jes2zSrQJSp1YSCc40qrL1TKEkW7WVKTHe111qNur59httTS1JK99a2KT0NFaeJJnazXG3CiLhoniCxCQtMnth4109Z87+UVvh9ul7mJgSFJaSCk3iNIHrXyTwjGLwO6vLIO8mYrZU3hXlyrJnXXw8a5tejKo9zb3p9RHOf/JhBE8xsEJmITcgz9Kbue0lySb/9oGHqjTuhaj+Ar5iOXjxdAS+SNx4DTaKybpxeZRQFQT3pgn51T8VS6h3p9MHfac5IAwnju1P/ANJz/wDzVN+077TnB17y8veDuBcUXfX2NNhm4uG0lCLe3JGdJzDUrSCmP3VKPhPGbN5muS0VBSmgFKHRI6An9PCaY4teBxrMtROckk9I6Voo2KUlKQk67Swg3+TeL4hwje8R2iUptbfEbfDQsmC4+6hxWVPiYQnT+961cXIb2JOP+P8AGLXGOOrU8P8AD7a0rdLqh71cNzqlpAnKSJGZeg3hW1L5bcI3GIucqOXqWHS5jN5c8YX7ROiklXY2h02GVqf/AKlfRpaG+G8ItbC2jt3XG7RqTJK1kCfQb+iaulWecIRU04pkJw7w/gPD1nhXCnC+FWtlgtospat2U93I3JUoz8RKwZJ1JSSdTUDx/wAIcIYRYX+P32CMXGNYjiSlWjgBzJUD3VCPAD6kVvVlatpxst22jLDRYSJkwBE+Z8T4k1KKwhrGLu2curdDibROYZkhQJVE6H/RFdWnJ0YpREUkp5fI5ZwngbEOMuNMOw99xSWEuKU8geCR3k6+OgneujsF4SRgTf8AVrC1SQIBSkaeZ86es8MYPYXdxiuG2iWnkuKAWFTOozGOneBEeCRU4y6lTQcOmkmaepWlUSyNKopPYggnFEvlasjKScysoCQT4nx/wpFzcYVZWl7dYm+yxh1sy65cruMobQ2ASsrJ0AABJmneLYnktXQ0pOb4UqJ3PX9PrVF+1VzKY5d8m75u4UnNiqvdHmC6Aq4aIlbIBUkqSs5GV5CFJbdWsSUQeXc1NctK6F8F6LbOLbjhThPAuK8X43dYW5gWBFGMWWGuqC1w+tSsOtX1glPaLb7JxSUgwknNBEVUOIX17i+J3mO4u+p+9vrhdxcOHdx1Zk+gk7dBVrc2UX3D/LzgXh3EL83WN8VWrnHvED5USp24v1H3TOfFNumSOhWfGqbW+lV12aAMrQjzmnpapPUzPVaSUYj22UpBW84JyydOlOrRRBU4rUkmo1DqidVHKNxT21fC1pbSOkyNq79lNR0x/WTBWjs2OMWZS/hrrmQFTIU82TulQG4Py/KtRtrh0uBa1rKpmZ1B3mfGt1ID7DjJ++CNPOa0i0ACoJIPSPGp49HE4SXVEWjzqRvnD96u6aUFlSS0QFgEAH+QfwFTdllStKGyO0IlRMEhJjvDqnTTXqa0jCFoTcJzkw4iCM20EiR8jW34awt4BD9wFrUkkuGSZ1MSR6UcKl3iUMdRLtOOWfRL2GueHLjgblPf8NcwOPMGwK4axh161Yv7xLalMqaa7yc24zpXr61pXMnmry3v/aQa4pseN8IucFZGX31u6StHwDw13028a4K4ixYoxBFupYLjDIbJA3gnX6UxN92ih9sS4rqFSD6157j9GN3eS8FLP4HR4TUlbUE11jj3n1Hd9orkmDH9IuFmN4zmP/tpufaH5LuA5eYmFmOpK0j8U18wUXTiIJJUCNoBooeKgAScsk6nY+lct26ibe+aPpyOfnJ0/wDtBwnQT/aK/hRDzz5RK0/pBwWN9bkD86+ZbVysrS2orKAITrOUfOlpW6s5iZ1hKgmf4fXSl7lB378D6Z/018p1AEcwsCjxN6gD6zWHecnKlQBTzE4fPpft/wAa+Zybt1SFJzkqVuUg6jwrHvAEKJAEgFIOun89KbuFnBPfbn0qVzm5U6lPH+Bk/wB28Sr8qQvnNyrbgK48weT0FwD+VfNlV6+2Q4VGd4JmvF/QBBVJ1VrASJnxJqfiyDv30PpCrnTyojMePcHA21uBTN3nZyrjOjjvCikmNHZH1ivnYLhzLJJBIgHOTp+HjWFXThHdPwkZjJmPQ1DoR6B378D6B3XO7lWFE/5bYcQDBCVkmfpTB7nvyqSNOMLUjYwhZ/8Axrg33gKEAhGszEdfGhruihBS3Kkq3g6io7lAq8l0O8hz05XKEp4qtyP9BY//ABr1cHB5yBmeUD516o7lB8YkWteNrt7d/wB5unIbIIyqJKjO2n5VWfMG77bDlkOqT3vhKYPgPyqx8Sul2qS1aJTcPyAVuGAkbkmqi44uXVIeYUpsnMVKyKzAwfGuwmmjnm0cusbXi2EmyeWvt7eBKd1J8f58K2pTDfdLjqkDNCQT1qq+WNypjGWhJHbAtzO2lWx2KsuRLmVzNrpoRWKcdLLU8oQ22yl7tNVJAzkeMedBUQlztinKCrugagCnrds+pspWmZBE6waCbVtTykBMOfHB0B8opSRutT63M4eczJOZCk9DMg1YXO9hrEsWwHj1pCez4nwW1unQjRKLhpAYcQT1P2SVk+LlaOtxtQi2tIUAUwJOu1b8tC+J+QnZrXNzwbjBURExa3YAVPo6hoD1qY+tjxGXqv7ysrdtN5cdmGJKilAA6Enf6VKvupVdrNslKg0QlCSYBH8ig4dbJYu7i9QkBdsyVoI/f2A/GhWGbKpxWZCgIAO5B6mhrqJkfdsXWispkycqU6AGetLWHzaOCO+tUJSs7nrGvn0pyltgJQ03aFHagEg7QdzWCli3CEsJCmmiSnOdVGdR6a0oAWnG0hsPFtvOB2igNEiKZLQnvpK3AlagoHqTO/pqKfPLYfShTVsltTiSlWc90761hmzVbJTbhORSUwVFUz10oJGPcaSu6cZAKQYERI2mOtIWsPLS6GgXUjKF5iAJH7ppwQ3cOOHtXg1CYhMSROYa7zHShqtRbraauHFBKnc5UtMQk9J9IoAZXAS203bsBLqozqlQJB8z02prcJdDye2UhzKicoUCB5GpFNw3ZrdcUtASpROUJOqQdPlUe+4oJKSkpYUQsmOm2poAxdthSiTADoABkRp4j5CmpbWplQ/s1J0zJ+8DRbtPu2RXa5h3fg3SD60N826kbupJVCgF6jzOmunnU4yA3LhWFrSpSUmEpKzJ0/DwoRUG0kLC0KAlJCdCZ6RvRkPBYKhAQVbqGuWAPn8qAh9XvC20QprRJUpMjfp1GtC25gNFBYbCnUJJXqYJkkbxUdfXBaSpHbFKT0J2p9d5fjDgSlrUQYJ1qAvcSw9Cu7/WX/uJQMwB9aujuLLwGd2HEWoeWoBLhKsoOpI01rXsQuS6W0A/BJ28amMUduC02H2wkyVEHpJ/5VrjrgW6rpGlaIvKyVkhhqVaKSrRPTxrbbJwrtu9qQMu/T+TWr4WySAkCSTp51tbDKgy2nIQkfGIqqpglPApKk5e+kaHNqJmiNLbbcyISSHI7pTI9aShhpb+RAIE5pPTSZodxd2toF5VBwkawNPSd6WMXIbPUYvOlm7cSDAfXJg7kwNflpWLxTl0+i0tkFbi1BtCQJKidAPnNMHb4XDyAACcw1671vXJTCVcQ84+C8J93D4fxyzUtv8AebQ6lah6ZUmtMniLZXjLOvPZx4ewnEvac4ufs2nHMP5f4RZcPWSXR/ZqZbDCgPVxpw/Ounrq+N9x7bYWl37LCrR3EFoiQVkBtIPQauZh6VQ/sT9nj9rzI5rlsod4l4heuAkD4Eyp3Lr5vdfCrq5at3WJo4i4ruS+P2reC2aS5BQWrfMnOnSZ7RbiD0lsVTRp66ueiL29MTZ8ABVcLdMSZGu+tTds6q2sVOrTqhKjHkNB+VRWCoyOLQdATNSl+pKGkNKUO8Up367/AKV0VzwimW5llCVWgRvpBM02Ce1ZSwkwFEg67Ci26ihJzCAdaZ3z/utu88hUEJJE6gT/AM6Wfoxcn0Jgm3ggMVfYfxNFiy4IaIKhp3UhWpjzIjTrXJftZYhwzxVzk5Z8ueYa30cIYniyReuWQSm5S8SGwnN91B7VrMRrlJIkgV0nY4kL3Eb66dQUqSoBKCTmjaYkxJB+nTavnl7W+MvcVcRM3fbZ2/8AKjFrJuDPZ9jaYWhYHorMfrXIpSdSbZurehDBFe03xzg/G3N3HeIuHikYWEW9lZBEdmhlhhDSQgfu9wkDzMVTdotTbS7hc/akhM+HjTjF1plu2bOWdCfAAdKQLYu5d8iB3QdgK3QWhJeBglLU8mUOOOrATMem9SdkAghRJBINNR2bQCQBNZTdKUoEDaujZvE9cmUVd1gm2HSRBjU6mtOuU9hePtpEJS4oD0mppeIZUhKd/WoN1ZcuVlUd5U71fxO5hWhCEegtCnobfiP7S5S2ttW4AIqVGO3WVKGllJTokk6kVAtZQApJMjzpanMy8s5jtI6VyqdapS9R4L5QjP1kLvrx+5uVvOrlcaqjekMPOFwDOrQanNTe4VLpAOgAFeYElRzAAiJmqpRUm2+ZdHlgm2XS8hBcJUuYT4k+OnpTnOtMhCgYG++lNLVQSzlKhKgIP+NHaGdWUqgJg5gR+tZhhzbdo3BzgrUdjAy/OKcKKVEqBdUpOhhYGtDQUNnv5gDJSSYn6dK889lWlbakgxBSJ9ddJpGk3yJQcOqbBlISDokD7vhI0msFShKgUlSZOZQGs6QKQo9qCnOHFA6gZjrOu1egFWQQQNdQRrrp6f4VLimSLUu3OQZUpJkSRJPqZpKcjgnSRqCkbnzEV5laJKVshQJiNRHSlANoSezQVAScxkeuulSAkaqUypRWtWoJ7v8AyHzoig4yciXioeSYH4UhI0VCQJ1gKHdP8zXg5lclyQIJIAkigMmXVttwhckzqf1pK3ZBJAOXY7V7MytxZcCdwIBiJrBACoLZAO5I0owB53K4QShWgjQH9K9Qcy0EhXaEkz3YivVGhEZN44o4jw0X71l7uXroqCAi2Ud51ECtIx/CrpWF3uKXLQYhZQGVTKdOlW0cJ4c4cLbOBcO3KMRQQpb9z3nF+O+grRuYt9bu4G625IuDChHdkE+FWxm1sivHVmt8uYOM2ySoAKUdYmNKuhaGBaJJCioRlI61T/LFkO4rbKKSQmTpvoKuRtxrJ2KHAo9pJkajT+NVVJankYjyq6QtLYKimcxSVa5Z2oYzqvA6+oIJEjWdOlPXUh24SV6HNAJEdKGwnM6yhSwk5wO74D+RUbYJCXVou0ZSsPqPaqkDqrrPlVhcm7djFL7GeX6nTHFOEP2CASCPeQnOwo+jgSflVf3qrgEIzqKRsopzAeAp5wtjasA4qwrH0pcbuMPukPFU7gKk/hNK8qOVz5llJpTWrl/Uj0C5s8Iu7htrN9s2ghYgkgEn8hWbJVjcErfZSyVJOgBVlOpHzrcOeGEpwDjXErSzUTY3d2rEmOzRADdylLyQPJIcy/8AdrR7Zi+RHbLQyHBIgz4fQmKsx16FLTg9L5olmbVDi1qS6QkoJJSkqjw09RSGHGLdsMIBU8VEkZZTtM+PSiW1k+5cB5krZkpC1BRGk6/SkraS3mu0NJXMIASTlGm9VsYWu5uCosqSFhSClCJgAD1603Qp63fSnsAtLYUgqyf2mo6+Ov4UtVoVW5NupCHQkLyFclYmSfIQKAlKwl0BbTiySClJJjoPwAqAPMLzXBcuG0pZWlQzKToCRv12qOWl657Bpb7VwgGSPhSJ8/GnS7TtLQqClsgfGDsPEAjqf1oTecW6WfdkqUNWwZlKfXqaABuqW0taUsNJCTlDZ+8emsa9PChEPvrypYU4Vd5akaBKZ2HoDXSfs1+y1ecxnUcdccpXh/CbajktzmQ/iMJ3CtC20JjMNTCgI+IT/Pv2T8Dw9H+UHJly4U6FLbdwcv8AaSI1UwpRKpEE5VEzrlMgJNE7iEJaZMujb1ZrVFHILja3HUNdulyXM8dpCYGlMHEpQVh1CR0kmRO+gG5qUu7F9hamn21N3LKlAhxJSQU6EQQIMjbxpktaLZxKFhZhM55Oquoj5gVoTKRuwttCcgYKyBKcyY0/hNNCq9fZULVkoTmgDNlEeZ+dPVOKS3nccAWASqU+YIB86e8K8Oq4ix22ZW4E2wUV3DsQkNJ+L5mQPmKaEHUmox5sWT0rJJcHcpLjGwMV4suyq2dP9VtG1FKVx99Z3KfACJ3233PE+GsN4HWcNYwplpeQFJQhKUq8RAGuxn/nWw3ztuLXs7dhRDIQlhCYAEHr6CNdRBMa1A47iH7Z7O1uD9qkZitR10Agjx0/T5eusbeFDCfLHPz8fYcW4qSm85NE4u4R4Oxx1XaWblooJKQ7akNkK6KKYyqTMkjQzOtU/wAUcucY4fC79hYv7AEEvNphTY/vokwPMEjzFXTeXcPLtDkSWJAAEhSpOb6HWoFWKlq4UltZbWlWVWXuzG46eNdKjwy3v06VVaai6rr4ZXJlErqrQxOG8fAqjCUKzI8/wra2pclKYgAAHL5amn2IcO2pd/auCtBtBEv2qPuGPiQP3fLp6bRdxc3bSilhxtKkjvCDKj4T0FeTvuH1rKu6VZfb0a8jr0K8LiCnH/oRc3C0p7JpvKkiFn97/Cox1CFK+LXwNYvL5+7OXtFIWnUpO/y8RUVcXWItgoLgI8apSUdi3mLbJTiELRoDKT001q3PZgxhnBOeeA42/GTC7fEb8ydPsrB9Y/3ZqnsPeddddLipyIgSep/wmrS9mfAXOKeeGB8LtvrZ/bLGI2KnUaltDti+hSvkFE/Kln6rJjs0d3cjOHrrgT2YuH8K7LLi3EqTepaBMuLul/YfVstSPOuhsDwa14ewKywG1gos2UtZwnKXFffWf7ylEqPmo1pWFmzx/mIixsWW0YZwrbJV2aBlQl0goZbSIiEgKIjYpT41YYt3VkqKCOp8/lVlvHEW/EmfRDOyhF8G07rEx4gRNO8YvXE3Vu32bakZVqUQIyQRl01mZVr5UXD8OdDzt4pACyMo6mmtzYkYsp5x0lTrTbeWfhhSj/8AlWlRTkmIEUQEkKnYRBrWeNcTZw7CHVkqK3SEJ30E+noZ8jWwXBDeaFaKOpIqueaF+WbWxtWyCtbqVgToSdE6eBEjpqRqOtF49FJ46mm2jmW5rGN8SWfC/CeMcQYkpKW8JtHrxTSlABxLaJCZJMlUZfGVDSRr87OZd0+hXDGD3jhXetWDvEOJKP8A/NxRYcB9FWqLVUeKjXWPtGYtcOcGYTy3w26Qm943x21wsqABJbDgUtXTZZtgD1BI9eOOZWMMY7zC4qxu0byWz+LPsWSN8tqyrsmEj0bQgfKsFtHCyyy6nnY1ErbcxFxbqQQ2nYnaTToPsuJ7kaeFQF8pRuozET5xTpts2tsFuk9o4ZTrsnb8f0rZkx4HThDqjKoFIWUIAJXqaA0XH1wSco86yslRMbCm1MhoV2iQYA08abRneVr1+lEoAJdJCfhJ1+tLJkoMCTCUqgDSacBPYtAI1Wo60NJSIgCCRuB4R+lYcWkHMDtt9aUkblUun1+tOWGyuCnSTIpiFFSyDprUjaob+JSNY0kT9KBsvkSdrmWoSooA6U+WEmCtCoOgEHf8vCo5pULUmYTA9d6eJdKkpzpbWfM9f50rPPmOHS4UpIb7I5fihAJ+h+dLblTyU7rBOxjprTdKkZsqGwrWZT1V9NKW2ICw4hAgEgqj9KUkcwS6V5Fa6EnakiVMLhnMVE6jQDz0pAWtGVAIKSP3tB01rKUdtCUqGoBKsxgDX+NAC0ZsqMgBkT3VAyZ86yt1pQ1RBCoif1ikht0EFGbMRocukadfWvLS5lC8wGRU6HT/AA+lAZFoKipKXElSlAkECPn4VhspbV9oiJGw0pKwVqzqdjMN41IrC0JUI7UJI7xTvPnNAZFL/tSv4jpKfOg5ytJGyiqAJpa09mASCondQ6aCh9iNMykqjUQdqAMgKUJLg9fGvVgJZVq4ET5TXqMMC7+IsRTa3K7ayU5fXCRmW4k91Ag6lUaelU7zGWpNu0HFQt4JJHUirWxJq0wxl2wbBU0hC8y80dpIkZj1g/nVK8wrz3y8U4iciF5B/PpTR5kPZGx8n2+zuXbkJns2ozRMSf8ACrTeDzyEvNtNiBMgAEK6H01qteUDQVb3yFFcdin4ehmrEY7Mo7ZSlo1CEg9VUjSbAbtLvH1hbaszqIKkBApakKypSpaA42AvPEZTHlThbLpWtZebiNEnuk/P1rCWUvuEISFJQIWobKPhQSAUq6UkhayCsSev09aDcuttoQyEFUrB8/rTpZcbdU0p0EpJgBPypulBeeSlasgJ0UVRr5TRggsvmci5xrgTgXiq0CluN2TuAXqiJQ0u3UFsgq/eWh1e/Rv1rQU2lw253ilYQIKRrt4GrD4Xee4l5ececFXaXe1wY23EuHobHdKGD2NytXkGXCfl5Vo2HYbf3I7VpF6Ae6gptyQpOsmapoyWlxfzXj3r8Gi64Wqan/Ek/t5P8UzAUGrV5F064kJkJyiSCY0PzNActhbpCve1pUtadFkjMAdTA2HrUsOFeLLpambTh3E4WMynHLV05iOs5Y1pVxwfxYhSixw7ibjhRAmzdGp0JkiN+lO8dCnJCrQu0fd++tCilboVoBt8xTdt9tV07GdZXoVpVExsdPSp97gXi14tPnhnE5KQFpQw5r1g93aPCsL4P4raay3HCGJFZMf9EWCgHXw1iJpQDcE8IX/H3GGBcGYPcupuMYu02y1ITmLKZ+0cIIiEIClHySa764e9n/kFbY7fYZZcDWNwvBBbuuOXLzr8uqzEZgtZSfgkpiCDtFcx+yDw08xzotMZ4iZdwxOEWT79s6+wpHbuOFLJQCqBOR1aoGsJPSa6Iu+Krbhuz5sYrhLb7VyrFU5VLSQAk2jQzJ8e+lyAOs1zr6s6bXgst/czq8OtVcZi+bwl7W17smrc7+ezVg/ccM4HdtsYW0EsIWwDLpEhSE5RonYTtv4RTPkbjF/xC3c3q3Xbodu6lO+hbgkiRpKrhQ9EitLw7lXw3xJwtinMDifibELPCLbERaWrKG0ocUgNoc7XMsGQc40gGQZPhZfKvijkzyV4XeTa8SYheh95x8u3iEyCoiQgJAAGg8dq8825VFUrSw3uexm6NG3dK3jqxty5v2mlc7OQ1pzRxB/FsMtU4TjyJHahJDdyB/74J2I/fEnoZgRyzxFyX5pYI85Z33B+Kura0L9jbLumVQQNXGgoD5xXR3NX2qmMQvl4PwqUttqhXatgBSp3BV/CoDgH2srHhK2vLTGsBdvrl0hTTrbwTKog5p9BW63u7ujH0Vqj5nHuuGUK+7emXkcq31gLJxy1vEqbfQoBTagVKzeY6Gtz4NYOG4Wn7IFV0rtFpII7oMpHkMuvTceFF54cbW/OPjyzx4WTdk6AGbtTKoK2hqJPVQAKQf7wHSkJebTbENjIoDRKTsTAj89fpXt+CRVzBXElh8seZ5DidN21V0M5H+KcTHsH8yMmfupdQdgNiYnQ/qa0/GuJ8Xw1HvK2Q/b/AAy0AFokdNNRrTh58KCkCO/oQdiNP4RUZi3ZG2RarkgJ+9qPL0/wr2Ls3jQnjPJ+fn5HA7zLTwQDvECLu5F+y8osvDsVSBmSoTlUfMjc+XnTbECtL6X8wKnRClBQAKht9RpURiWHvYZcKW2lSrZ74wN0jor5UM4sv9mvJuCGXLQyVFIMxqAmQdT+RnSslK4nRnpq7Sj19n+OX2l0qSa9H1Sfs8VUysKSYKYhQ1MedNMetDkVilgke7rjtEAf2Kj4f3SdvA6eEwScQhJcCwrNrB0n0/hT+yxrs0wohbSu6U9CDuCDvWriN1Sv6KjU5rk/AS3pSoVMx5Mgb1eaHCnUbEbg+NMnb52MoQlQ2MjetuwfgDiDjbiTD+HuCbBWI3OLvhi3twsJKFmScylEAIABOYnQAztJtHl37EvMrijGMQtuYN1/kHZ4c6bdT9/al9y5d3hlCVJStEf5zPlJPdKtY8RcV6dtnvZJYO9b21a4eKUclB2a0CzW+lGUrWZ9Bt+M11t7G/AbHA+AYp7QfFduW1qaew/htDsArJBTcXCZIMJSC2DscznhT6y9gNVnxbgjVzxyjEeFUul3Fl+5Kt7sNJGbI0kKWlRWQU5swKcwMKim3tZ81bY4aeCeDsPVh9uhKMIsbBpsoLLSe52aUDYwIjxqqjdUrv5J58SyrZ17SWK0cYLU9nT2jmr0YjfMITcF/Grld4CIU4yUoS2pI6d1CTG2/jXZeB41h/EVkziOG3CFsuiSQfhPUHz1rkj2Y/YywfllhLnG3Nvihb98u394uMJs3stnaISlSiHXE951aRqchSkEEDOIJu3lhxDwy/aYlfcCu3QsXbk5LK7MPN5ZBUlOpyqAEAkqgCddB2tEZ0lpWJL7jLu92WPxrf3+D4NdLwQtquwClDi091vxXA+KOg6mJ0mo7BFvu4U1e3zynXVyorVuTAGsb/yBoKjmeIkY/iD1g4se7oYQF66Kzz+UU8wts2dh+yXnCty1WQIjVB1SfTcfKljGUIqMuZDwYxB0hCiSoJAkmdI86o7m7e3t3jnuamwq2tglCVgEAwQFDp1HjtBG2trX+JO3GKMWbaTlDqS6Ohb3V8oFUFzg5gWuH46WLjEW2be2S5d3jqTIQlEqJ0EiI8Oo0rn8RbajE2WuOZTXM3igvc47J9b6PcuWPDl7jym3YHZ3i0wwNNCVOqsYg/eMaa1yRdRbWrbaSe6nc6yfGrR4s4pv3uBcRx/EHFJxPmXjRuVpUrOU4XYqIQlKjrkVcLygeNkPDWp8QWYOY6BNLTWEZ6ktTIxLfvF3nUe433lH9KM9dIdcJUDAGnlRrazduGE5CEpUMyyDvRv2SwkfaEk+JqxIryN27pskBICRSnCkCEgQetLVb2bJG6vlQVKSoykQKlAxMgCTSERpqAP0pSzCT9KQlZQCQCdNpqGCCJ7yU5QdRtWFqCgANIpB7y8ueAOvlWHV5WyATtFQSBakqkiQdZ3qStFA6khJGs61HsREneY+VPLdXeBIkg6T40PdbEp43JVKgpxBCh4TPX+TThmcyjABjMDHgfypm2swFNxmnTXb+Zp226pAQ0lYKVaK8VeE/wCFZ5LCRYOs5UFEwCO6mGhqf5gUlvMgltxJSYEEA7T50JoLEryaH4STv4aev5UZKCy6ntUaORCZEmajC8STKl98qzQfJFF7ZKguX1wdB3OnnOgpvCVOlJ+7100I9KICjtUjtQoAgydTHgf56VOlPkAZCHAE9mEpSkQBoQdd5O1JXnQlTaUDTqCf415TL61dp2DiwdEqynTzpu4EEkJJlJ1E1CWp4QBUJQlteckyO9Jjw6UiGkgStSQROx9etKztKSFEmY0AMgmkqcyvLJXHyk7UaXnCAygIIWErJURm1mBpQ1LeyKCgCgD7oH6UvVSy72pVm6HcGgBaNEGQk6AbinhHcDKXLaIK1QIAjwivVkOLAHZEpT5Aa16nzLwI1It/Evc37R++u7llA7QS0V99YJAMCqc5gYq3id8G7ZhDNs04UtoTuROhJ6mKtbHrCzwjBrjFbbCEWzKFqbTcXb+d50xEJQdtetUljt21d3jamWylCe6JOvrVUFuQ/As/lAi4aYvFtHTIkK8966a4P9l7m/xrZW+K2PD9rZWFw0h9h/E3gz2gUAUqypClwRBnLr0rnXlU24zgdzd2ykZg8hJBidq7k5e8QccWfCWDu2HGN422bNlQadGdCTlG2o/GvF9s+1MeytvTuJxypyx5rbPI9DwDgj43UnTjNRcVnfOH06EZg/sIY88C/wAR8cYbbuLEqbtLVx9IPkpSkflWz2XsN8DNNIaxXirGLnIrNlt0NspPyIWfxpPMDnxzM5f8H3nEycQscR90LQ7FdsEZs7iUbids01W+Be3dx7iVuXXuFLBaw6W8qXomADPwedef4V2qvO0tJ1uHxnKKePmR3S85LxOve9m/2TDvLhwx4+k/cXHbex5yisilxTGLXS0/eevIJ/1AmpBn2YuTVsW1I4PStTRlJcu3la/NdVD/AOPBxKgKD/B9oVpnu+8HXX/RFFR7b+MqCC5wXbStYR/aEkE+hp69tx2ryhVX/wBR90zJTnZQW0ofc/7S7meTnLnCffbiy4Zt7dd7aPWd04hS8y7dwQ4iZ2IqpeRHDfG6MKvjxLhyxYWV0/houXLlvKl+2eUwuO9MKgR5trJjMJiLH2338RWq3VwvaNuJWpBQoqB0nX4tq0N7j9tVzcv4dj/HWFsXb714WMP4juW2EuuLUtYS2FwnvEnTxrZwefFuHucLqhUmpYaeYtprZ85ddvuJrU7K6S/fxjj27/gWPxTyo4xQw0nC8WtmHcGxFLfD165jDbRcbcOf3aSuQptZlMjX7QzJSK17E+VGPt315d4rxFg9jg2MrTaY1ZOYy0lNpfSvMWSVR2iSlTrY00U40TlzKrS/6Qmn1pTc8S8yHlJP+d4uvO6dY2c3j868eOMKcBKcQ46fGsqVxliSYPho+DXe/a15yVnU/wDz/cYVwyxby7mP4/0N2wrkZzSumLO4wri7AzjWHNIucHvrfFJNzZFWUJIgygCSgkERmbMpy5dl4X5J87+DeLrfirBcUwdy3v0oGNYXd4it1lzLplbWpBUUxCkEnMiSiVAZl0bdcc8OqbUpNhxW8qYKV8ZYsJ1if+ketQz/ABLwi44gOcK4y446ZlXFmKL+ZJuB+tKuKcRlt8Tl98P7yyPDeGreV0vul/afQNWAWlyE++MM2wUgFYbcSrIojUAxOhkSInwrXr7l/gYwzGMOTxRijSMUQGllV32mUBWaUhaVBJ3ExME1we/inCjx7nDSkpTqS7j+JOlJnaC/qah757D8i+zwO1YXEgLcW7M7QXFq8KaFW9r7VLdpPxcPdJl8aHDaDUoXayvoz/tO3b3lDgF5w5dcMvcyeIEWF242t1gXbSkdwQnLmZOTwOWJAE7VreJezjyov8NVbYjxnjilIEpdGIr0PQ5AjIfSK4kas7LtV9ldXlqZzKbtn1NIM+ATt8tKfgtW7SrcXD7icpKfeHi6snwBMnxOtdGFrlZawzm1r/Q9NObkvuX4i+PcPt+HuI8RwbhLGUY3a2Tha99bt3GCSPiCkuAEEeRUDpB6DR28c7C6XbOXTQuQZKFrGcyJmK21tVq3lZ92Sj7372cx+6QY0rUeKOBzjb6bsPJYuE9xCSIKhOkgDYa61ohax9VFD4lUzqaRt3DLLzjK754kqePdI07g/ifyFSl5eJU9K1SClGg0jT9KVgtqzbWtvZtqJZaQGgTuQNNfofnTZ5tbynJSAQSTB+Efwr11jSVvSVNdDhXVaVxVlUfUaqKe0b7hA2VrEzUbevvKPaI1SfFWnpUoEOFlbgOiCYXOg1idPn9BUfepPZHKsgAwBNdqnWljZnPlDDIC9fWe46yevw9I3rVeK7dHYi9QUAuKCBH3gnqB/wB41tNxohxecgeI1I8/rWocTreStVu6FBDHcSSmATOv4zpWXiU4ypbrctoLDItu6U3vq2fiT+o8DR03BBgqzSNDOh86i+2HjpRG3T8B2MwfCvP968YN2lG7cvuJWcD4xwbELzEruxs2b9hVzcWq8rzTIcTnUg6wQmY0+u1fQbg7nLwpYYkOJ+Mbu4SnGWkfsa1DfaJDG4fX1BUCIkfDrPe0+YSX1NGc0CfpXd3K/iPgnhWywfjrj+3ZxPGbzDWbawwhi37RrDmykZARsVhIIAgBIka6EeW49TUtNR89z1fZutp10ny2fu5nUFtxRhWNrTcWz5yq7yBECDWu8UctOX3FmP4RxXjXD1tcYzgV0i7sb0FSHW3EGUyUkZwDBAVIBE1rGE8YNXsu2Puluhw5g2GlICfUdD8q2my4iLLP26GX1ET9kon/AJV5aFerQkqkG1JeB6urb0qsdE0mvA2lXEmIWdoMONiy/al5ClDMASjMJBCt9M201X3HvMLg/lRhrnEeC3JdaDqW2stu6pTWZRzFWhhKcpGbQajXx1LmlzVc4aw9NsolzEMV7Ziyt7ZYLqFBB+1VCVd1CiknQkkgDqRyejGsa/ZzGHYbxNiNuEtqaTbquylpCRGUQohI3VB30Ar2HCuI8VuqXfQUcZ3ynmXjyfuPI8StuHWVTuXqzjo1s+nM7s4S5/creLWg6eJbCwxG6yh4lYSFkbHKohQ1J6VYt5xtwphztgXOLLJa3EKQ06VhLb6TBCQSe+RvptXyGvcfWrErlxtvsQp9RShCoDevwjyG1eVi7r4QO1UMpGUeB0mK9R8Y1JNo87mKex9gMS4kwFrAbzG7S9t0pYRkKwsEAqmBMj8dvOvnZzU4hxHmfxqnhHCH0DEOIsQyurUfs2LRBPfUr7qAE5yeiWzPnpdp7QXMlrBF8I/te3/ZJKu0YbsWGytwpylxSkIClrIiVKJJgayBWv23F6sNteI327dS8Q4htPcF3ufvW9ssjtkNDoVpCUE9EZ0j4iaw1YupUc19g8aiS0rqMOZPE2HcU8WPPcPoW3w/hLTeEYI0vQiyYGVK1CBC3FZ3lj991Vafizn2Y8SNKmP2XaMoSGLghyJGcZhPpoKav4awoJXfPl59wwAkkJAqVHCwUt5eSKtbK/KEntFNpgRJiKeG4ctwEFxVwlPxHSR8tTTlzDbdtYUi5dJH3SrQfTWh3Ab+8DmQSQqdfrv+P8KnAZENXjD4KmgJ8OtBeUDEISmfCguIaccztuAE7KGyj4GvKcJ0WO8nfzqSAaySoADQkVmcxCculYUqSSdAKyVAFOWDA8OtIxkLURP4x4Cm76iYA6mi67EfEKbuqKlwDttQAZCUhKYGtO2UpMqPQ7Dz6U0QogJ6GKcs6iD8QIoAfs5fiWZGmWjpAABIKgjbYz4UBoJUdUnyP5zTplspgB9IEyMwVHzgeNVzksYGiwyUFIHfBUYASNdzsfpRM4lLi2yrLoQCYI9OleSvIhxCiVdR3t9d4OtJUpSkdojMU6TtM/M1SWBEhCe6W5Ck58qhr9YmaGlSS4QuUBuRCgf4VhZSFEoSpWYRPX1H1pSUoSopJBIE6kfWKZLDIZkO5viTHzE0gFJXBb7sTpudKUYK8qkp1Gh3FZYW2lajK4TA9flVrykSZAZVlgBIH97WYpA7VTsITmjUa0VshC1KLainVQk7E+UUHKQouJHwmQJ1M1EefIhtC1BSVABzc7UPOmClUmCDooAdfGvZsqgSlSYIgb+JNLeX2yw40kN7g9DpT5wyNzLeVSAVEA/6Qr1IW204rNnH416qu7l4hnyNpxW04jx7DG8R4oxZrI20VsWiVgqSP73mYNVleqQ5iKUNfAk/WrR4s5j/AOWGD3L+N2FgxiDbrYCrNhLJeSQrMVgaKMlJn1qqmR21/wB0GM3hTKOh56C5+8vPlkx7twybxpxPau3WUIMGUgA9a7T5fL7TgrBiVE/1RAkiDpXHHL2zUOG7RttCSHluKLitMmsfoa7B5XJ//QWDd7ME2+WfHvGvinw1rPCLd/7n/GR9A7AvF5VX0feiF5+IC+U+OgCcqbdWvk+3XKnC4SUqbUVD7UEFPiRFdY882wvlXxAka/YtH/bIrlDhGHjcM9oEqStBE7TBrJ8DT/0NRfTf8sT0Hbdf6B/Z+ZJJtkh+e3WVJWAoHUid5FP7G3fYuve1DtlNIceGUQNBCSfOmd237teKD7qczxBWRJTJ6istXVyzhL7yHO2SllZzJBkNkga/U19v58j4+3uJu28IfQl9xRTdZCIA28QSKDh18rsUWOR1tKl5iUpOc9O79airDH7BxwW6bNx5wnVKknbT+H41KpxVhx4OJs0NoQkiImBOhqWujIySiMUU046lvO2kqUUZ9CTvGnlpQ2S6685dXSUQ68iFJAJAUYJ18I/God6+as3E3DIcCtcy1DNIP7o28vlWP8pEKKVpVkU6Q2pIEjf8DUMZPxHl5cve89mlCwlSz31d0ESdRr4dKBiKmVJTbMdoVITCgE6jWCfOmNviiVB5u6fZDSTDaCNCCIOv0ohxW27T7JtvQgShUbnT6UreGS8Bxe26EQ205lnKFLMkkSNR0pqLrOmQsqUysaq9YzeUjxoVxcpauIcULYL1CViSJ039daA1erZWtB+2QFA95ITqDJjx60qWSM+JKtvh1VxeOhsJUlLfaFKlkkn4R4HTp4GmUKFwMjWVSAIUsmJiNt413pg5iiVKR2hcyJJUgFUDN0MjaKc+9++dpc3Fx2iyoBKAUqMTA1IPjQ01zJymLS2XXFrtmSslOYKkyYPQCthb4YcHDF1xTiClMBaw1aNp3eUVwpRJJATAXHiRO28JhzFxiGKWuH2rqUFa0pVlbSpWSe8dBuBNblzAubhxjDrNtpLdsyt1ptCSR3kJbAT4GAd+kmt1jRVR659CqrJpaUQdgpDbWqwsJSoADoo6T+P4U2t1oJIIEZSPCfOlkqbs8rSDmKgJHgNzQlp95cSylIbUSCTB16zXoKfIxvmwiEqDRaWlJAACSTGu+vnqfnUJipQntDIVCYkeOnhUleOLTmU0skA5TA18d5+XzqBxV8QUTrmMgdT/ACK2UVkpmtyMSQt5sOL7qnQV/wCgNT+FaPjbq8Su1uPqUA6pThTmjUmTW5vOKbbuHAsZW2FCCfvLhGnnCiY8p6VpN8Upfgnbak4jtTwPR3lhDe3wRFwkltaxHnNOmuFVvlIbu1Cd5amPoad4WppslZKZzCQTuK2qyDCR2zCzqjqoHf8Ak1xoUoyW6NEpuK2NbXy1xFaAW8WslEiYUVpMfQ1Y/KDizjTlhxO9xFd2FpjDxs1WLQdezIS2sjNAVEGAIPQZvGoFN66hYiV9I8qdM4gXPiGwiNjSVrChcRcJrb2j0L2tbzVSm9y6f6dsHYcS5cYTe4c9GZxxRCmhPgdR+NP1887O5aDtribMRpnAjTrA3qhVXwX3HJy6wZ2NRGJW1rcguhGRQOVS2zlV5ElO/wA64dfsvbtfu2/tO/S7VXC2qRTLP5l81rLG28NuRdum6w+4Kg80ClSUKEEJIIM5sqhruInU1r+Lcy+GbLB8+CYwt/Eihxy4cctyp15akOIAU84FGIUnMkQFTOhSmKlxXB71IL1vevOJSdApRJG/hTbD8NxbiG59ysnkqudvd3XQlRP93MYPpuPOtFpbSsKaorkYLu7V/UdZrdg27iVEkkyZmn1o8VuoAgCRPkOv4U2x7h/GeF7puxxthDL6kBfZpeQ4Ug7ZshME7wdYg03tbjKhbvkUp9ev4VqTMeB/Yuly+KQZ7Rwk0td2V52E7dCKZW1wm2QXEn7RUx5Ui3dg5iqZ3oyGCRbcLmRajBiNTSXX0qUlX7ppoXcpUPnSUuEpWsq0Gg9akgdF2XRKpKjlobj6EZSDPeyq+W1N21lbqTm+GVz6Cm6lFbZSdT4+dD2AUtJbcIgFKiZTWEukEod70Duq8vCslUhM9U0NZSR5nSoJMDvK1nXwooJCdQI8+lDAKFDKSaUpRAE/OlayPF4ErdV00CRE0BOsHw1ojp7uUdTNIRM6CTFBDeQ6ATG21OmMoSnuklR6U2QCdOtPLdOukHSYoayiBy0Fg91MAaHNoPwpwkFSYOZWgOsaD0mhpUMkkHXTQ7fhThpOXUpXlnvEad2oksolZQZDq4KAFax90CRttNLK1Du65FKzQobetZDgjtW0kHKBsdPn1/navKIdM5CSQDJ2VPlvVWMyzEsS2BNuZkoBcgJ08J/k0ZsIUkuQJmIUdFev89awhK1qV3A2lA0kVlSj2YQ2rZObfQD/ABNC9J8gzjmJC3AtRXJJE7aDypOQqWYGQKV9BSkpc7JKhqsk7ER1+uxrKnHWidAARuKsy8bC8z3fQtSVJ2MBJ38tvnSUIcdUE5AkkSZMVkukICgqCZkpPTSkODKjMomNiZ3/AJipjnG4gQKARlHeySTBBk9KH3EOELACQPXrSgtCWi4o5vu+ArBBWO0CCkkRB2NSNqPJDcaKI+VerKCzllSlJJ6BJNepdcRsvwOoB7BiCSBzCSlcGc9iBPyC6Y2nsEvW9yu4d5iIUkGU5cOUZn/v12aCoMrLamW1MryKGXKCE7mSSdJ1I8aQzcZGSFqKSrdCWtfoDCfl9Ky95JkaTnjAvZjVgGHM2T3FBdDIJC/dSkkEztNWzw3gKuG8Es8FVcm4NqiO0KcpVJzbfOttWq6S8W3g+leWEtpQokK8QVCCBoZnr5aR98pC7hS0lRCgkypIBmPAbV8f+GbfglF/7q/lke67Byxf1F9D3o0DnQgK5ZY+NdbdJ/2ia5r5Ocv8a45/bKMIetkGxcty6HyQDn7SDoD+5XTXOBAVy04gJ2FoT9CDVJezPxAnBLjiq2StAdvRY5GysJKspfk7gmJ6a6iuV8DlTFpVS/jf8qPVdtI6uHNrxX5olVezxxu6626cRsEhJzEICiI8pTt86LYcmuOsItn0WDeGXLjiphT8JySTG3j+VF5o+0Angb7F1bzlzct/ZsNK7NZAJhZnUCQddZ8KobGvab5j4sFhjGfcWnARDCZVHmtUmfSK+5KUuZ8d0lxX/Kbj66C3H2cHtnRCk9m9lEjXfLWn4hyf46Sp1XaYH3j92/EpHXpVNXvMjibESUX3E2J3CF/Eld0uD6iYqIVirilFa33Fk/eJJj6mjVJcwVNFwucpuZjQJ/8AIbqRsDiSdR9KgcQ5Y80WiXWcKsnkKVJ7C9bXHymq+GJozZkrC1FMETShihbCEocMk6walVHF5RLppbZNluOCOZTZ7V7hhco1lNw2df8AWrFtgXMRlSi3wxdFSxBIyqkDpvWuqxTOorU6sz90neloxZQEpWtsdCFHejvMrdEd2vE2tXD/ADCulIeveF8SWUHQhrMQPCm72B8coUqeFcSQFfF9gZqAHEt62kpbxC8BmCUPKEfiKco4t4habzp4jxYIAkBN45A/Go7xk6fMcXFtxQyCw7w/ifZJOczbK38ZihDFcUS2ppzBL5BSmElbK5j6b7U3VxTjzqlZ+IsRBVqR70rX8abO8SYykQcfvlCdB27hA+hqe88iO78yxeWrl1cYrcX93YvsJtmISp9BSVOLMdR+7nrYOJnksXpQ65nKElxWbcyQNvkPwqF5bu3b3Dzz+Kuuum9dKkKeUpYLaISIJOmoc2olxf8A7TxF55MhpMIRAEaJAJ9NvpXbtI6aaa6mSo93kUm4UtvukOCCBIynWSfzols8SlTjoCDGXXeeuvrQr66t8PZ7RxEE6pKB31/wFare4zjlwCbdghkSSkCPl5mupShLGTM2kbFfPNuuKAdywnY6GT/IrWrktKeCS53QdSfLWol66xdwlT4WgqJVEmJ/maKwLlYDaW+1U6ckT1Ok1soyjHmVyTlugGJKUxbFAlIfcLhSeqU90f8A3Z61C6Od5ULEHTUHpWwY7doDryWnM7bSQ20Z0UBpPz3+dawSTuax8WqqKjTXtLrdZ9IfWalIKkoWkzGk1KN3CmkkFJ0VExUAxouadJfKVBSVkEaaaVxo1MGprJMDEXEq0cgJ86O3jCmxkchceP5VBm6UZCxmB1M6zSu3aKRKSlSlTorSrdbFa3Jw4ynq2An1mayjE23lakArTlOm5GoqCK8+Xs1BWbSCQn89Kx2q2cijKJPXrFP3gulZyyVUsKZcyqAHdP41HX+Hs3RWsAtuJIhSPPx8qU3cJ7JxJXospy/I0V1eVLqtfhH5ilmoyW5OcGu3Vq+1KXElQSSMw2mmxUpsAR12rYnwFByRIBQr8YNRt9bttv8AZj4VajyrNOnpWUPGWdhh2uvfJPgKWm5SgRCj8q8u2iI/GhLYckkFUeVVDjlL+frAPQ715SwUZZ660yyOpM/EPWlBa5goVHlQA6S6E5o/dgUgrykEfOgdqBIkiKzJI3NABycwnSsAgxJ08qGFn4QmaXoNNqACSCIjveNYWZ0mT41gaKlR/wAaSTJyp1JoAQ4ZUIMilNzp60giCR4GiNAg6kRQA4RPTr5U9t4kpCTAAMjWmSJMDz+lPG8yUZQtKR9DQA4AayBJSTrPSniENlIAbUEggyevlTVqJCh1BiJiKcsgJUogmQND1/nSoayTkMhLi1iAUZzIEAkgCfpNKWlASRKQM2WAfxoSFrC85QtIAM9Ov40QIKVCVAIKvMyYmq8tSGTwhQTlWcygUpI0I260guJkHQoO/kPSvGTmUc240jQUkhMRIG4Ghk60633J1BFlKFdmACoTITsBSHAUEkoOkGPGBSlkQFpSDIkjwpKSpZUpWRJOkEUwjMiFFTBToQYJ6+FYdUhKUsz5GB51hKAhagpMiNDm3okDYxptrvQCeBOS3W2EEqOWYga1gxEKVqnUeVZQpQ1dSlQIB+deXCVZihI6wATIqEsPJAglE/cPzNepLikIVCUSCJ2FeqQPqndY2zh+HjDGmUKQpaHcyntMupMxMSf0qPZ4hVa2yLddr7y8ltx5WRAkpmBCzuTt1323it7LiPFbTEX0PLC3FIDSSgureX3oiAFGAJ+Uz1rN/j6sOxG4uMT7dFwpoFcvqTlTmmFJy5jBCd/D5jmOTzhF+nYs9rE0Itw49e2qykIBSgpSrZUoIT4fCY2iTQmb+3xJoXtsmEOEmD4yZ/GqjtOMLuwaZakocuX+zzKIWlI7pJCVJJUSmY1jc1YfCF21dYGythedCXXWxpEZXFCNh+Ar5N8MKb4FDP8A5Y/yyPa9hVjiM/qP80RvNvXlpxIOgw54/QTXLvK2+fsWsacYulMlxNu0TkUtMHtdYT1AnU6RM11LzWTm5bcTD/4XcH/ZmuKsMddaLwbuQ1mKNyACoSRqduo+Zri/A2/3FVfT/wCKPXdscfsyf2fmjR+Z+K3mM8c4pdXNypzK6GWyoxCEJCRp02n51qmdwd3Nm+c1sXGiXTxLdPOFK1O5XM42MpHU1AyFolKRIOo6fKvvaPjiG6lOjukECZrPaPHSSmdvT50aOhR0mIikrSZ73oI0o5kiEOLbTlCxv5VkPrRqlZkVklURECYpB131owiRXbXCjnzKOXWKIl64JzZlR1NZ93IGhgjXSlBJEtqVPhr50uVjKA8LgwQDqTJgGKKbtcELSCmeo/hrFJabQpRzSoRIPlWFNoIKwk67nNP6UmMSwBgugjUAz1SYNKRnccS2hEzA0NYDZKw2kdOhmPOpBm27NBKVFJVAkDX607aTxggsvg/PacL2jStClJUROUFKnFHX5GpBOJ2PapRaozKBjNHkelMLCxvbzA8PTahSGS0guEArJ7o8Pl4U9bt2cNPYWloQSJK1qA8tOtehoRaprBhquKbTHF3Zth5N4o9sFZviTOUdIppiGKMus/8ARkNFegLZ3H8xWEm6tVOupucraoKgpJKfxj8Kgru7b99WbNwFoK0zDcen8711rWUprdGSol0EFJvHuwICkqMSBEfyBWLhgYc09eMOOAIhlOfTK4pJkjxhIUdPKl9tZ98ahZjLlVlk7kCo/iK8Nph7do24YZbLyh/2i9dfRIR+NXVYttPHn9wqeDT8afSp/sUKkJABqMjvEzSO2UtRWTJJkzSswM9K8xc3DuKjmzfThojgUlUK22pZc7wUKCSQqT+FKKhPrVIwUOEyNf4V4uKmAR/GhjTasFQBAPWp1MBwNRtFYzrbBOeZ6HY/LY0LOqYzHTzr2YzJ19abWwCIuEmA4mNySNP8KcguOJUhtxKgogQTBI3Hlv501ytuE70BaHGVKKe8jcppdTIwsjxTjyC4cqh9nm1BG2vWkPqbcfbUsAggBQmhtXrjYIVDjShEQDHyOlZuF2zznatKKEhKQNTuPGT5dKMthjcw4ytk5VjukkJPjB2/EUCpG5vH7zDGbAdglFuHDnLcLJUUGZGp+ED0qKC1Elteix+NIOZXvKdxSAZ9aUSRIPWhL7pzyfOoAWQAJjzputRK4CgAaIpcpzJPTahoSCcx1J1igAzaShHjSioJHf0J8OlJGYaAfI0rKDJOtQ1kBMLWCACBWQkIkyTSppK82U5Y+dGN8gDBkT40VvWZGw0oaQdJG29HRAQY3O1SAZuc0bDfyp83mcAEmI06UzbA1mflT1KClCCFZo6zoJoAKgIC1JAAA1AIPSnLZXkJEadZ2pqg5D2iiBOsgeP8inSACClMkJ1nSCqobS2JCDMs5nlEwZKRuKWlTKc3aiJMafxoLZcyq7/eCgI30pZeuCMmc+Ig/Wq3B5ygFNFbjkJbMeO2lLz5AEKMAgnTTX+YobYCSoJ1XqDJisKUcyUrOpOhI1EDap3bwxk0gjziSJA7x00JgChFObMtQKiZINYS4sqE6mYMgVkpuE/e32V4TTpYFfMUE6SSUnaAazKsgGYCAfimk5SECVQdRPkKWS4UfFm00nrUkAgXtSUKy6b7ddaUpaMpyAaCCQaT2y3W1JVJju+YoDhhxSk7A6jwqFnqSKUjtIV2iduqq9QcyDqU/jFeqSDsV4lQWsrfcuOx7VxTN1mhHxKUY12SPEHWk2jCsQU88hJXlObtO2a7wIymZPdG/wB0/SK1l/FLK5s7m7c/qzikiUMoSAVfFA1MT4HT8aCMSd/Zr7i0JTqQ2tSIWF/DlhG/oRE66dOSng6HdeJtlk4q0tLhK3Q2bUqzJZfSlKxmUMqlpUjMRlAOpGgjXSrh5XNFvgyyQUZe84oDtM+hWTorqNdDXODOIJZaLiVYebk/ZqYLIUUkrMk541ykDQ6gnrXQvJ65uLjg5CbkW6VM3LjIFukJQAkjYDbrXy34Xcy4BH/2R/KR67sQnHiUvqP80SnM1GblzxMn/wCE3f8AwlVww32JdKbh9DcDMCoHUjwA8p3ru7mEjtOAOJExvhN3/wAFVcGuBztQpvQpk5s4TH1Oted+BqeaVZfTX8p67td6XDZ/rqjU+Lw47ds3C9HFNgElJGYAmCJA6EVr6VIUkhIObzFbfxKq4csi1kDgZcLmdIB3Eb/ztWo5kBUozyR13r79F5R8axgwAonLpIGkCkuJVI00CqImTJGYK6mOtZAWUlSyDl8EwQflpQ3gkHlEpMSfTSvEKUdSlPe6DaiAbkqJAkVgtt5u8o6gdNqjXvhEmBlTqoiBvqdKWDLhgkAfSaWMkSpCVKG4IMR50pZ75zABMnQaRSOWcgDKSTKn0qG+Xy/jSsilKyCUhO8DpMVnsm3FgAEzoJMxToJSkkJCStIj5zFRl5yBhtgtEApChvmP1innZZYdyiEkKGkxAEDU+H5VhtsrSgpaJUQCTEk6mfPpU3g3Cl1jF5YW+dU3lwi3ZSTC3VrISEpBGuu51AqFz3BJvkbvglyzh3DVuHS8FmAEtCVKhIBgTpUZiPEKXwqys03FjeAZv60nKXIGw6TvpvVtW6ODuHME94RhtzibCFm0RcNKCQpwKVKgHFoSpEJbOYGCF71F8R4Vwdjli04MPes/fkdrat3RSFOIAdClKWFrazFxCQJUFd7ymvV0KieIJ7o5dRYy2Uhd4pibjotr1xQnqDoo9aj1Xt205lKhHTTpWzY/gxbDYQVLlsudZQMxAnr061rblo5cJyyQlA1X/e6V0FrhF4KViXMctvtqV/WBmQyQ64DoQNNj5zHzqGx+/XcWt1cOKKlOEkz4k08u/eGGDZ3CAHQEuLUDuIlI+mvnIqAxdz+p5BP2iwN6a4uGreUvIIQepIhUviNd6WLlI0mBXktNxqkGlpYbO6N/wryh0TAekbn6UtLgIo1rZtvLhUAJ6eJ8K27hvBLV5w3DsJbanLKYBV+RiQYpZVFAMZNXYwzEbgBTFk8rw7pAJqVt+C+JLkZlWiGU6nM44kDT0mrVwXB0XjIDlulZCM4WVQtUHSCREEA6VLpsmEXTqlW4lptIPaIjIoyTOU+cAeXyrNK7fRDxpZKjZ5dYwpSU3NywgH4ssqy+E7fyalmuV6Q32j2IOqGYAhDQHnEydYqwk2qUKeacdUhySE51FSiP3Y0y/CTMRGs07FvbhIQFnKTGYXKQoZtzB1Py8IpHdTfIdUkaNb8s8BKEQ5eOZtVZ1gaeUJ1FSY5Y8NMJS4q0W7mGmZ5R08xp+VTq4zLBXmShKUsqUqUp8diQTFKYfVcMPqU8QUR2ZKpnUbCddz/hVbr1H1GVOK6Gvq4K4StyptOCIzpkEFSlD0gqryOFODexWUYEwhwEEJczDNO8VOi5tQyLcMvIz6BRXmIP728jbbWmibFy7AaedHZtyrMqJ9NTTwqyXNkOKfJEUrhbh4pPZYHboWoEpABlIG8if40pfB/DDsO/slgKmZKTInpvU0LdDSElS0Zlp7pSNUifH0pkQ6ftHVLAQI01jwNXK4fUWUMjI8EcLqBy4agyRAM7fX0oKuAuGitSF4Y33TqNdNehBqYUq5cbKionKQMwHxafgK8yv+sALckFJPwhUHw116U8blZ3QvdsiF8u+FyQ23hrehlXeVp+NL/o54WSJasG3NdU5iCPxqROIPh8K7JCkpMZjAI86wi8K71ZWstqO4CpA102qxXMU+SIdPxI4cvuFswcXZ5RP3SVR+NZXwFwsU9oxaJ0Md5JmpRL5S+A2PtArvJozy3A8klRM6lExVyuY9YoR0m98kS3wBwqlRy2QCoJAImgp4F4ZW4EHC0KIGpyA1PJcZDpLizC/BUkH1rKkysFIOQeJqxXFN/NQrotdSCTwFwyowcKay7ghINYTwLw04lXZ4a2kpMAFAgmp8ON6raC8qNI60XIlwNqJWRMRmiKsVWm/moXupLmQH9HvD5KUpsmgo6wBp8qWeXnD625S0gn93WQa2Zln7RJUQ0hsykkSDRGW3r5/sUONqk7gRr400JU30RGjY1VnllgpyqWypufvAkiPPworXLLADmSHngEncKmfwrcA2uzztXRStxByjwrLN6e0yi3bIJ+IiIp13fgg0mnO8ssHaAz3l0CraY28KH/AEW4bKUpv7hECZyjUVvyHQlz+sFMTCev0p8hhh1wlxRSDtAEVZGFBvDigal0ZWY5T2CkBDWKOEqVJ7okR0rKuUVr9y+fzbI7o8KsVSCjuoSFJBMwNfWlt3VsBDi1adTsKnurfONKF9PPMro8obFMFV86SdT3RE+lGPKaxfGt07I3GgrfITmU52kp1y7RSA7EFSlIA31Emn7i3XzULqmaS1yqwptxKBcPk9QCNfwoyuU+CLzEXDoIM6RP5VuzFxbt53A6oTME03XcMl3tUOFMHXXenjSoZ3iiHrZqH9EWCkryXT40mZG9AXyhwEJ716+FHfrW7ftBtcpSSQBpA3NITdtKntEz0mdqfu7X+BC4qeJp55L4E6cwxF8/KvVt5xdtn7Mxp4V6m7m3/gFzLxNTexG9esW2VXbyW21K7mZYRp98CdwOsUZu8dUlVq6+8C7uMq5ynUkiJMGCToZBr6ms+znyNYTla5Y4DERPuwJplivsr8gcUSO35bYUhY6spKPyNfOlcze7j+P+D1uih/E/u/yfMBoXtw225C2y2ChzI2C2UjQFQIJE676n126W5AOLXwOsOPhxab5wKIEQcqDB08+lX/f+xF7PV4XCOFLi3UvXMzeOJj5TUnwn7L/BHAuGOYPw1iV+xbOPKuIeUHCFqAG56QkV4rt9w667Q8I+KWcMz1Re7S2Wc758zvdnru24Zed/Vnthrk+uCouNkdpwVjzcfFhd0P8AZKrgPEVsttS6lyfulJEAjoR10navq7xByGXiWDX2G2vEAQbu3cYCnGNsySmdD51yhxJ/4O3mo00o4Bxbgd4cpEOJU0SD01ChXnvg24Ff9nHVhxGGnVJNbp9Hnlk9HxritlxKwqUqFRan0e3VeJxpeAupU0V/ZwZlMHXcTJ/jWuqbSFEBqdYnUGuh+KPYq9o7AlKWeBHsRQ2ZDlg+h4adYKgfwqpuI+WfHvCrrjfFXB2LYU4SRN1bOIBM9CRrX2WFaE/VkfM5WdePzXj7/wAjTVICVgyUzBgnQGjEAhW5y6Hx+lHNl2akt6g6zmV/hSE27kSlYURBiJB8NateWUd3IbtleZUk+JO1YLRBKwEhKRuDR221yQDClASkjfX+frVt8K+yZz24x4cVxXhPBakYamPtbt9DBgiQQlRCvnHWkclHdsaFGc/VRT6UEKygHMrTSNT6UpbGYf2igf8AR0H5Va1x7MvOqwStf+Q93dIb3Uy627m9AFSa07G+AeNuH3T+3+FsYw8JMFTtq4gfUj9aqjcUZvEZp/aiydncU/Xg19jIUICIBVl1kTOo6a0ZKlOS2sqWhIkAA6Dx0HnQ8oTlaIVmEAlSYk/WauPkr7NfGnNe4axRq3VZYSHQFvFWVbidD3RrEgaEzuIkVNetTt4a6jwhKdtVrT0QWWaxyp5cv8yuLLfB1LuGbFtCl3NwygqyAJUptAUoQCtQCBJ8fCrk4W9nq9sbRjjPii/Ns8EOpatV2Kbi3ZASUhLy0vBTWh3KFBO8kiugcE5VcHcsMAdwaysi17y0UuqU6VKW7lIKif3j4+Q22rU2cfWlLnDF1fXqHLQJZChdKy3LQGi9diR8QHWvJV+0znUatk1jxXuPZWXZxKmu/wB8lJ803sbwe9ssLNsu0tmkZ21tuF5p9xcKUTunVIbAACTASYkmtaxPiC8/aOF4DdMsqubC3atlJbMtrWtZcVBJ3BdykbgpOsVaGMssWmKu2K3VPW96lT7CnJWpp9JAUBmnxCgd5naqZxvAnMDvnHru4D1rb57oPKXC8yQSlP8AplUDrvPp7Xg3ElexxJYl+a/r4nmOMcHnw+TnDeH4r/HgyDxvHW8OLuJJzXeFvXIbXI+0typSiCjxRAVKfHaKLhLaMQwV6+tjbPWy1odelUKaRlExpBgzImegrQ7u6WbXsXVEoOZak+I2H61DYXjN7gd6pVvcOIQ5LLyUnRSDofwr0Xx6dJqLex51Uk90bPjGI2dxd3VwxIDrilJJ6gkkA/KB8q1S/fDroaSdECdPE04xG7Ut1a4CUqUV90ACTuABpUc0nPKjB1mqrq9deOhLYeFLS8hUtd2AkwaMlsgwAZjXWstIA3GY+dFQhQQFAkTuY6eX89a5rnjJpUcDvDUIyBRSTmOo/npW8YGylvD1ZipKROg0BJSNNPIH6CtOw8wlKWhJKZEnQfzpW32bik2jSVgFATlAg6mVefe3rPUk3uTg3LD7xdu2stthJCFJSku94JKQBuPGZjx6709VithZuu27jQWpxHwhUSreZE6npPjWpsXZthcAAoSSCSG52BgbmDP6aU4euENIbcYUkXKyVZ1kKypnSBBjp9KzuO5YmTLNwXXFWaWnWlqSR2QBUrQbEzImJJ8/M0dCAx2jqiQgiGgsHMk66kaxHnprUfb4q4Ut9uh9lLaM4ecIUkq21zAncDSRQmLluWVOKBQvvvpCylWXrAjaNN+tKhh64py9uSy8+ShAKU9g2kAkGMxKvGT/ACabNvN2zp7FKyW2zJScwPeOum3zoScTcZUsqzqW+YLhPeCRoB5HXfekpu8PbUG2S5mSCChaBAkHWfkKZIXI6s1slQuLx1TZKdEpEkmAdPP+NLaul+8OLYcCgokRvoN59Kj03bDZdaWISFdopSNAZjYdYmlWiUhKu1gAJKW0z3ikDc/Mk/KmwRkerS0Fe8ONKCJBbCp1O3XYQB9aX2bLiUh5xI1Mga69P0pqBcPpZUbhJlQTkUnLlHiT86LcpdtHy2UhbjeqUo1kxQAu7S2FuIt0rkamDuemlIbaS0nIpZBUATpJnwoTd242VPrQELCgoAJgFUbGlu3CmXYeRnSO8pSf3vOgBK7JJfNul453EgpTFJuMPU0RnQoObHvAH6U4RiCnlpezKQlCCEmBKvU0AOqKVLKSpSjp4g1BINtpxkdqEL2+JWu1KF68ETcJ7RMaAaH605Y7FbKi4D2kGB40hbDUICUKCN1KnTx0pssjA2UtbrQT2KkQZT4/WnqLoIT2biiekRXlFCBISSnoa8w04tanVREaSNxTa2GAjdysudmhBAjeKdocUEwRA3mo8OrQ5m+6dTG/pSlBTiF3GZaUAeFPGo2K0P04mGEFgjMFaeNItrkl/MleVI17tRoeIbOTWToojrWRce7faOCCeoqyNVrYjCJE4k6ta0qSSgHRXjR2L9CZSCD13qHVepebIaWJ6g0k3bZZIEHKNwanvp88kaVnJODFhIcDZzIPwnw8aUnGnACFq0I0GatT9/K1lXbagHQU3Vflp3tFOyM2qSd6HXmlzGSRu9vjRShed2ARFN3Mas0sLbccOZXUHetTcvO2byNqUAfGmFw6W0kqdJI6A71MbqfLJEoLmb4MbaaZCFL6TNN3sdQ20FKWVSeprRnMUUtnM0ok9QaE5iSlqyOuedW/G6iF7tG+nFkuN9xZgQYmhJx9oCHFd6Iia0hGIrQsFtxWg8ay7fNuDMqQR5U/xyT5h3aN5RjTYT3F6+HhTW6x1RGRokHrWiDEnEr+Puz0NGTiveySYOpUKsjduQvd+BuBv1vfadqBI6mvVpv7Wd+7MDavU/xqQncn3UVIOVEAmsZiCUggnxrJAGbyFAVXiW8HcismXZIiYM60BRUCBmBAG9LcJihrQmaz1H0L4rBkLUTBdBHl0pKiF6FRjpBoauoogSBBAqvLY7SR4rKB3dU+FDuWbK+a7K/t2LhB+662FD6GidmgbCkKQk5gRMVOtpYBJZyuZVvG3su8guPVOPY3y8wtu6dEG5s2/d3Z8cyIn51zlzC/8Gjg90Xrzllx47YvRmbtMTb7VufDtEwofQ12qpRzqHhtSSpRGpoVzUpNaWaN5LE9/bv+PM+VKvZf5ucmeMGsW5g8FLv8Dw9K7lN7hsXTa3EiUCIlOsaqTAjWdjnEvaZ45CbjD7TC7ZqyfVmW27c3KnVK8VLDgST6JA8q+ryW2ri2yXDSHEqEFKkyDXEXt18lOX3D+Bjj/AMI/ZuJuPBDybYhDLuY6lSI38xB8ZrZSvXKqlVWc7ZX9P17CtQjODVJuLW/lt+P5lKcKe0hf9o03f3TlipOmS4UXWvk4NR8x86tS159PKYR72pXYO/55tYdaWCP3hI+Rrji2bStffEwKkrZdxhLqncNvLi2Uo94NuEBWnUbGtFXhFndPMoYfkNQ4xdW+zepeZ2Rw9xFwdiGMM4w7w5glwpIyqcVZtKJnxMbzVmYtxZaYS6WcNQ03ZlpLzaEJGVKVAKiB1Gb6zXCnBHGuNvY2izWpkSUy6hGRe/XKQD8xVz2XEWK3t6q3uX86cqUGRrAQBWWr2ZhOSjCbx5nVtuNxrttww14G/8AEHHjOI2tw1cvmQjtEkbAgajzqsMXxiwuilxjMXWySlbY2noQTIHpROKtcOdIMFLZiP8ARqsvfrlhwLbX3pAmt0eydKyqrMs5LXxiU1iKwbXiLz144i4ReJuDa95ISrvI11kHvD5itX4gfZvm7gPMBUpKCT1BFDxi5cvGEvOwHU/C4jurT6KGta3ZX11eC6auXSsNEJBO5B8a69/b0uG01Cit/E5tO4ndzaq7pmicQcM3aGVX1on7LbITqINaS+x3lEjXXumr9sLVm6vGbN5MtOrShQ8QSAapfiZCbbGXkMjKnMDFYrG+nWk4T6HI4rYU7bTUp8n0GF2ctuyg6KUJ840/WsWiCTPQ9axeFQuFImQhCQPoKNboASnU6ifSunU9U48VhhwlKG8uWVEiDNLyd4kEERsDpScoDyIEaJP1oyEBQcWSZBjeqMvkOP7dtSggw2AkQe8AY8/Gtjw98MttlTKZQRJAJlEyB8j+da4wTlQkHTKRWwKbQywwtCRmLpTO2gSD08yaSQEpbOtIS6sSStHwBkKSY2nw0G/SkBS1uQ24lSgrUg5MgV92IjxprZrSlzvNJWCgmCpXXN4GelGZT2oeXJRnIBy6dJ9etVZJwObZx9LpQu3aKUgx2kQCRvvpsevSlPXyVMl4toSjKQMgMfj6edItrh24duGHTmSBAnXQHb59aJdFduGmW1nKQsd4AwI6aaUEjqzeQptwMpBDYlZVB28j+dBecQtoONkJJASSoHx0mN9JpmwsjsHClKiQCZG80pxbiLojtVkAKME76mgA+cdr9mUSoJSlRhQI0g606XCHghK1LATqonQSB84FMCG02yHQ0nMEHxjSiMte8ICluLklR0NAD1Sg5cJSysPIGQnKoiI1Ig0dA7qn+1Kc6ysAnWKhgtTKISZhUa+tODcvPFsLUCA2rp4UAP1XynSWrdILaUhBkTrO486j7kuOqSkJWSTvOlFt5TZpfCjnKd6zh6ii5QoBJJ3JSNaAPNKbaUlC3AEkZTrtTxlYfAaSkrk95UUwuBF++noFV5F0/bQtleU5ooAmVuJYSUFsyk+EyKI4ttxttK4SkHod6H2inUpUsyR16mhsQ5bkqSJSsAHrFQSFS4sPqbJCwNBToIcJyZI0iAaavf1d49lpOpryXnG1dqlXeoBBfd22m1KKsp8/Go595aE5EvzGpAo+M3by7YyQCBIjxrXGbh5b8KWTmOtX0aetZZEtia9/bFutCxEDTzqMVfqCVJWFRrINNrh9wlRmMogUBQzJUVSdK0qmlzKXJ9Bz7yXklSXAgp2pKrhZBCnQn9ahe2dS4UhZAmjOrUopJOtN3cSNbH0htsrSsTOs70j3ht4BbkAzt5UzcJzr1OxNM0KJlU60d3EnWSS7paEEJd06CgqvlOJhahI28aZP/GaGgnJmnWd6rlTXNE6iRbuWUoIIg6xTdbyXE51ESTApo+ohsqnWRQ0KJbTrtBpFHKyOOnLoMqyka0QXBV1iajCSpwlRnU0VS1BZg7Jp+68wHqnWtdUz4UIXKUKKYJnam5J7SOkTQHlFKjBiiMMoB8LiOivrXqjlKIMA16juvMTUf//Z\" width=\"500\" height=\"300\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo2`
--

CREATE TABLE `modulo2` (
  `modulo2_id` int(11) NOT NULL,
  `modulo2_nome` text NOT NULL,
  `modulo2_nome1` text NOT NULL,
  `modulo2_nome2` varchar(200) DEFAULT NULL,
  `modulo2_nome3` varchar(200) DEFAULT NULL,
  `modulo2_nome4` varchar(200) DEFAULT NULL,
  `modulo2_nome5` varchar(200) DEFAULT NULL,
  `modulo2_nome6` varchar(200) DEFAULT NULL,
  `modulo2_nome7` varchar(200) DEFAULT NULL,
  `modulo2_nome8` varchar(200) DEFAULT NULL,
  `modulo2_status` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo2`
--

INSERT INTO `modulo2` (`modulo2_id`, `modulo2_nome`, `modulo2_nome1`, `modulo2_nome2`, `modulo2_nome3`, `modulo2_nome4`, `modulo2_nome5`, `modulo2_nome6`, `modulo2_nome7`, `modulo2_nome8`, `modulo2_status`) VALUES
(1, 'Home', 'Quem Somos', 'Projetos', 'Blog', 'Ví­deos', 'Contato', 'Áreas', 'Equipe', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo3`
--

CREATE TABLE `modulo3` (
  `modulo3_id` int(11) NOT NULL,
  `modulo3_nome` text DEFAULT NULL,
  `modulo3_descricao` longtext DEFAULT NULL,
  `modulo3_status` int(11) DEFAULT 0,
  `modulo3_foto` varchar(200) DEFAULT NULL,
  `modulo3_imagem` varchar(200) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo3`
--

INSERT INTO `modulo3` (`modulo3_id`, `modulo3_nome`, `modulo3_descricao`, `modulo3_status`, `modulo3_foto`, `modulo3_imagem`) VALUES
(1, 'CMMSoft Soluções e Sistemas', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div id=\"comp-jkrqepl5\" class=\"txtNew\" data-packed=\"true\">\r\n<p class=\"font_8\">Com alta qualifica&ccedil;&atilde;o t&eacute;cnica e vasta experi&ecirc;ncia em desenvolvimento de sistemas, nossos profissionais desenvolvem aplica&ccedil;&otilde;es e inova&ccedil;&otilde;es atendendo &agrave;s necessidades de cada cliente e parceiro, com cria&ccedil;&otilde;es de um simples portal &agrave; aplica&ccedil;&otilde;es mobiles. A ger&ecirc;ncia do processo de desenvolvimento &eacute; monitorada por t&eacute;cnicas de gerenciamento de projetos de software baseadas nos altos padr&otilde;es em desenvolviinova&ccedil;&otilde;esmento de sistemas.</p>\r\n<div class=\"col-md-6\">&nbsp;</div>\r\n</div>\r\n</body>\r\n</html>', 1, '601551233562.jpg', '1511373144.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo4`
--

CREATE TABLE `modulo4` (
  `modulo4_id` int(11) NOT NULL,
  `modulo4_top` text DEFAULT NULL,
  `modulo4_side` text DEFAULT NULL,
  `modulo4_side1` varchar(200) DEFAULT NULL,
  `modulo4_bottom` text DEFAULT NULL,
  `modulo4_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo4`
--

INSERT INTO `modulo4` (`modulo4_id`, `modulo4_top`, `modulo4_side`, `modulo4_side1`, `modulo4_bottom`, `modulo4_status`) VALUES
(1, '<center><script language=\"JavaScript1.1\" src=\"https://t.dynad.net/script/?dc=5550002911;ord=1509196195836;idt_product=21;idt_url=325990;idt_label=85223;idt_category=60;click=\"></script></center>', '<div style=\"margin: 0-3px 16px -3px;\">\r\n<script language=\"JavaScript1.1\" src=\"https://t.dynad.net/script/?dc=5550002675;ord=1509199567429;idt_product=10;idt_url=325990;idt_label=85223;idt_category=17;click=\"></script>\r\n</div>\r\n<div class=\"call-inner\">\r\n   <h4>Anuncie Aqui</h4>\r\n	<a href=\"#contact\" class=\"btn\"><i class=\"fa fa-cart-arrow-down\"></i>Clique Aqui</a>\r\n</div>', 'Publicidade', '<center><script language=\"JavaScript1.1\" src=\"https://t.dynad.net/script/?dc=5550002449;ord=1509199408724;idt_product=15;idt_url=325990;idt_label=85223;idt_category=53,48;click=\"></script></center>', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo5`
--

CREATE TABLE `modulo5` (
  `modulo5_id` int(11) NOT NULL,
  `modulo5_nome` varchar(200) DEFAULT NULL,
  `modulo5_descricao` text DEFAULT NULL,
  `modulo5_status` int(11) DEFAULT NULL,
  `modulo5_imagem` varchar(200) DEFAULT NULL,
  `modulo5_limite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo5`
--

INSERT INTO `modulo5` (`modulo5_id`, `modulo5_nome`, `modulo5_descricao`, `modulo5_status`, `modulo5_imagem`, `modulo5_limite`) VALUES
(1, 'Os que os nossos clientes estão dizendo?', '', 1, '1507217787.jpg', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo6`
--

CREATE TABLE `modulo6` (
  `modulo6_id` int(11) NOT NULL,
  `modulo6_nome` varchar(200) DEFAULT NULL,
  `modulo6_descricao` text NOT NULL,
  `modulo6_nome1` text NOT NULL,
  `modulo6_descricao1` text NOT NULL,
  `modulo6_imagem` varchar(200) DEFAULT NULL,
  `modulo6_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo6`
--

INSERT INTO `modulo6` (`modulo6_id`, `modulo6_nome`, `modulo6_descricao`, `modulo6_nome1`, `modulo6_descricao1`, `modulo6_imagem`, `modulo6_status`) VALUES
(1, 'Excelência que impulsiona seu negócio.', 'Escolha um de nossos serviços e solicite orçamento sem compromisso.', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo7`
--

CREATE TABLE `modulo7` (
  `modulo7_id` int(11) NOT NULL,
  `modulo7_nome` varchar(200) DEFAULT NULL,
  `modulo7_descricao` text DEFAULT NULL,
  `modulo7_imagem` varchar(200) DEFAULT NULL,
  `modulo7_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo7`
--

INSERT INTO `modulo7` (`modulo7_id`, `modulo7_nome`, `modulo7_descricao`, `modulo7_imagem`, `modulo7_status`) VALUES
(1, 'NÓS FIZEMOS PARA VOCÊ', 'Trabalhamos sempre com o que há de mais atual e eficiente no mercado de Gestão e Desenvolvimento de Software, buscando sempre, surpreender nossos clientes e parceiros através de inovações e soluções criativas com foco no crescimento e desenvolvimento do negócio.', '1551231316.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo8`
--

CREATE TABLE `modulo8` (
  `modulo8_id` int(11) NOT NULL,
  `modulo8_nome` varchar(200) DEFAULT NULL,
  `modulo8_descricao` text DEFAULT NULL,
  `modulo8_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo8`
--

INSERT INTO `modulo8` (`modulo8_id`, `modulo8_nome`, `modulo8_descricao`, `modulo8_status`) VALUES
(1, 'Parceiros', 'ConheÃ§a as empresas parceiras', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo9`
--

CREATE TABLE `modulo9` (
  `modulo9_id` int(11) NOT NULL,
  `modulo9_nome` varchar(200) DEFAULT NULL,
  `modulo9_subtitulo` text DEFAULT NULL,
  `modulo9_button` varchar(200) DEFAULT NULL,
  `modulo9_imagem` varchar(200) NOT NULL,
  `modulo9_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo9`
--

INSERT INTO `modulo9` (`modulo9_id`, `modulo9_nome`, `modulo9_subtitulo`, `modulo9_button`, `modulo9_imagem`, `modulo9_status`) VALUES
(1, 'Fale Conosco', 'Se preferir pode nos enviar um e-mail que em até 48hs entraremos em contato.', 'Enviar Mensagem', '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo10`
--

CREATE TABLE `modulo10` (
  `modulo10_id` int(11) NOT NULL,
  `modulo10_nome` varchar(200) DEFAULT NULL,
  `modulo10_subtitulo` varchar(200) DEFAULT NULL,
  `modulo10_icon` varchar(200) DEFAULT NULL,
  `modulo10_button` varchar(200) DEFAULT NULL,
  `modulo10_button1` varchar(200) DEFAULT NULL,
  `modulo10_status` varchar(200) DEFAULT NULL,
  `modulo10_imagem` varchar(200) DEFAULT NULL,
  `modulo10_paginacao` int(11) DEFAULT NULL,
  `modulo10_comment` int(11) NOT NULL,
  `modulo10_tags` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo10`
--

INSERT INTO `modulo10` (`modulo10_id`, `modulo10_nome`, `modulo10_subtitulo`, `modulo10_icon`, `modulo10_button`, `modulo10_button1`, `modulo10_status`, `modulo10_imagem`, `modulo10_paginacao`, `modulo10_comment`, `modulo10_tags`) VALUES
(1, 'titulo do blog', 'subtitulo do blog', '', 'últimos posts', 'Leia Mais [...]', '0', '1507658588.jpg', 10, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo11`
--

CREATE TABLE `modulo11` (
  `modulo11_id` int(11) NOT NULL,
  `modulo11_nome` varchar(200) DEFAULT NULL,
  `modulo11_button` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo11`
--

INSERT INTO `modulo11` (`modulo11_id`, `modulo11_nome`, `modulo11_button`) VALUES
(1, '', '© Copyright CMMSoftware.com.br | 2019 Todos os direitos reservados');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo12`
--

CREATE TABLE `modulo12` (
  `modulo12_id` int(11) NOT NULL,
  `modulo12_nome` varchar(200) DEFAULT NULL,
  `modulo12_descricao` text DEFAULT NULL,
  `modulo12_imagem` varchar(200) DEFAULT NULL,
  `modulo12_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo12`
--

INSERT INTO `modulo12` (`modulo12_id`, `modulo12_nome`, `modulo12_descricao`, `modulo12_imagem`, `modulo12_status`) VALUES
(1, 'Vídeos', 'Vídeos Recentes', '1507991160.jpg', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo13`
--

CREATE TABLE `modulo13` (
  `modulo13_id` int(11) NOT NULL,
  `modulo13_nome` varchar(200) DEFAULT NULL,
  `modulo13_descricao` text DEFAULT NULL,
  `modulo13_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo13`
--

INSERT INTO `modulo13` (`modulo13_id`, `modulo13_nome`, `modulo13_descricao`, `modulo13_status`) VALUES
(1, 'Time Gestor', 'Conheça os gestores da CMMSOFT Soluções e Sistemas.', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo14`
--

CREATE TABLE `modulo14` (
  `modulo14_id` int(11) NOT NULL,
  `modulo14_icon1` varchar(20) CHARACTER SET utf8 NOT NULL,
  `modulo14_text1` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo14_descricao1` text CHARACTER SET utf8 NOT NULL,
  `modulo14_icon2` varchar(20) CHARACTER SET utf8 NOT NULL,
  `modulo14_text2` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo14_descricao2` text CHARACTER SET utf8 NOT NULL,
  `modulo14_icon3` varchar(20) CHARACTER SET utf8 NOT NULL,
  `modulo14_text3` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo14_descricao3` text CHARACTER SET utf8 NOT NULL,
  `modulo14_icon4` varchar(20) CHARACTER SET utf8 NOT NULL,
  `modulo14_text4` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo14_descricao4` text CHARACTER SET utf8 NOT NULL,
  `modulo14_imagem` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo14_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `modulo14`
--

INSERT INTO `modulo14` (`modulo14_id`, `modulo14_icon1`, `modulo14_text1`, `modulo14_descricao1`, `modulo14_icon2`, `modulo14_text2`, `modulo14_descricao2`, `modulo14_icon3`, `modulo14_text3`, `modulo14_descricao3`, `modulo14_icon4`, `modulo14_text4`, `modulo14_descricao4`, `modulo14_imagem`, `modulo14_status`) VALUES
(1, 'fa fa-car', '100', 'Carros Entregues', 'fa fa-star', '230', 'Clientes Satisfeitos', 'fa fa-pencil-square', '387', 'Horas Trabalhadas', 'fa fa-coffee', '156', 'Xícaras de Café', '1552272074.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo15`
--

CREATE TABLE `modulo15` (
  `modulo15_id` int(11) NOT NULL,
  `modulo15_nome` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo15_subtitulo` text CHARACTER SET utf8 NOT NULL,
  `modulo15_button` varchar(20) CHARACTER SET utf8 NOT NULL,
  `modulo15_status` int(11) NOT NULL,
  `modulo15_imagem` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo15_envios` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `modulo15`
--

INSERT INTO `modulo15` (`modulo15_id`, `modulo15_nome`, `modulo15_subtitulo`, `modulo15_button`, `modulo15_status`, `modulo15_imagem`, `modulo15_envios`) VALUES
(1, 'Assinar a Newsletter', 'Assine nossa Newsletter e receba nosso informativo mensal', 'Assinar', 1, 'news.jpg', 3000);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo16`
--

CREATE TABLE `modulo16` (
  `modulo16_id` int(11) NOT NULL,
  `modulo16_nome` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo16_userid` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo16_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `modulo16`
--

INSERT INTO `modulo16` (`modulo16_id`, `modulo16_nome`, `modulo16_userid`, `modulo16_status`) VALUES
(1, 'Instagram da CMMSoft', 'cmmsoft', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo17`
--

CREATE TABLE `modulo17` (
  `modulo17_id` int(11) NOT NULL DEFAULT 1,
  `modulo17_nome` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo17_subtitulo` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo17_button` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo17_imagem` varchar(200) CHARACTER SET utf8 NOT NULL,
  `modulo17_paginacao` int(11) NOT NULL,
  `modulo17_comment` int(11) NOT NULL,
  `modulo17_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `modulo17`
--

INSERT INTO `modulo17` (`modulo17_id`, `modulo17_nome`, `modulo17_subtitulo`, `modulo17_button`, `modulo17_imagem`, `modulo17_paginacao`, `modulo17_comment`, `modulo17_status`) VALUES
(1, 'Páginas', 'Nós amamos escrever', 'Leia mais', '1507658588.jpg', 10, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `modulo_aparencia`
--

CREATE TABLE `modulo_aparencia` (
  `modulo_aparencia_id` int(11) NOT NULL,
  `modulo_aparencia_cor` varchar(200) DEFAULT NULL,
  `modulo_aparencia_idioma` varchar(200) NOT NULL DEFAULT 'pt',
  `modulo_aparencia_favicon` varchar(200) DEFAULT NULL,
  `modulo_aparencia_logo` varchar(200) DEFAULT NULL,
  `modulo_aparencia_rodape` varchar(200) DEFAULT '',
  `modulo_aparencia_slide` int(11) NOT NULL DEFAULT 1,
  `modulo_aparencia_pelicula` int(2) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `modulo_aparencia`
--

INSERT INTO `modulo_aparencia` (`modulo_aparencia_id`, `modulo_aparencia_cor`, `modulo_aparencia_idioma`, `modulo_aparencia_favicon`, `modulo_aparencia_logo`, `modulo_aparencia_rodape`, `modulo_aparencia_slide`, `modulo_aparencia_pelicula`) VALUES
(1, 'yellow', 'pt', '1551231095.png', '921551235313.png', '121551235313.png', 1, 80);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagina`
--

CREATE TABLE `pagina` (
  `pagina_id` int(11) NOT NULL,
  `pagina_nome` varchar(200) DEFAULT NULL,
  `pagina_imagem` varchar(200) DEFAULT NULL,
  `pagina_descricao` longtext DEFAULT NULL,
  `pagina_pos` int(11) DEFAULT 0,
  `pagina_area` int(11) DEFAULT NULL,
  `pagina_data` varchar(200) DEFAULT NULL,
  `pagina_date` date DEFAULT '0000-00-00',
  `pagina_autor` varchar(200) DEFAULT NULL,
  `pagina_description` varchar(200) DEFAULT NULL,
  `pagina_keywords` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pagina`
--

INSERT INTO `pagina` (`pagina_id`, `pagina_nome`, `pagina_imagem`, `pagina_descricao`, `pagina_pos`, `pagina_area`, `pagina_data`, `pagina_date`, `pagina_autor`, `pagina_description`, `pagina_keywords`) VALUES
(1, 'As Maravilhas da Tecnologia 4G', '1510661905.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Conectado como se estivesse no escrit&oacute;rio</strong></p>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 0, 2, '02/10/2017', '2017-02-10', '', 'ConheÃ§a nosso blog! Em vista de procurar um novo mÃ©todo de interaÃ§Ã£o com os leitores e leitoras da nossa pÃ¡gina web, produzimos um blog para melhor', 'portifolio, design, clean, 4G, tecnologia'),
(2, 'Blog post title', '1510662003.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 0, 3, '06/10/2017', '2017-06-10', '', 'ConheÃ§a nosso blog! Em vista de procurar um novo mÃ©todo de interaÃ§Ã£o com os leitores e leitoras da nossa pÃ¡gina web, produzimos um blog para melhor!', ' Portfolio, Design, Link,Gallery, Video, Clean, Retina'),
(3, 'Em Magazine', '1507326124.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img style=\"float: left;\" src=\"../../../../images/blog/news-2.jpg\" width=\"350\" height=\"239\" />Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 0, 1, '06/10/2017', '2017-06-10', '', '', ' Portfolio, Design, Link,Gallery, Video, Clean, Retina'),
(4, 'Portaria Virtual', '1511374808.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>em desenvolvimento</p>\r\n</body>\r\n</html>', 0, 5, '22/11/2017', '1970-01-01', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE `paginas` (
  `paginas_id` int(11) NOT NULL,
  `paginas_nome` varchar(200) CHARACTER SET utf8 NOT NULL,
  `paginas_imagem` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `paginas_descricao` longtext CHARACTER SET utf8 DEFAULT NULL,
  `paginas_area3` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`paginas_id`, `paginas_nome`, `paginas_imagem`, `paginas_descricao`, `paginas_area3`) VALUES
(3, 'New Title', '1511530524.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 5),
(2, 'News Pages', '1511528059.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 4),
(4, 'Page Example', '1511530560.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 3),
(8, 'New', '1511531714.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>teste</p>\r\n</body>\r\n</html>', 5),
(9, 'New', '', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Elit sit, parturient in nunc dapibus. Nec? Porta porta, adipiscing odio habitasse, a mid? Scelerisque aliquet tristique et tortor porta nisi montes pid magna, dictumst magnis et porttitor sociis elit montes pid! Ut turpis etiam adipiscing. Elementum placerat. Dictumst, nisi, etiam, facilisis! Nisi massa! Turpis scelerisque integer urna lacus dignissim mauris nec duis, ultrices velit elementum? Placerat urna vut, purus odio cras aenean dapibus arcu parturient, lorem augue, mauris et est placerat, et lorem. Aliquam lacus ridiculus augue. Aliquet nisi, magna amet tortor lorem, pulvinar habitasse proin sociis, nisi eu? Purus scelerisque mus auctor et duis, aliquam tristique natoque montes, montes turpis magna mus ultricies tempor sit auctor integer. Penatibus. Nascetur et, sit, porttitor magna sed porta amet nunc vut.</p>\r\n<p>Adipiscing porta in pellentesque cras vut mauris tincidunt augue! Placerat non. Egestas? In augue? Nec rhoncus turpis cras porta pid? Pulvinar dolor magnis adipiscing magna nec egestas ultricies diam. Adipiscing mauris mid. Ut enim, a, tristique natoque mus pellentesque, adipiscing ut, montes augue turpis enim proin lorem! Eu aliquet, dis? Dictumst ac integer ut sed, egestas diam enim duis! Eu dapibus, vel sit urna, proin ut tristique phasellus in nisi hac non phasellus integer pulvinar? Mattis, et ridiculus proin? Odio dignissim in. Sit mauris proin. Ultricies, habitasse? Et ac est et lacus! Ultrices, porta habitasse? Tortor rhoncus nisi turpis, aenean, nisi ultricies mus scelerisque. Nisi augue facilisis? Proin rhoncus rhoncus, lacus in ut? Dis nunc ac dolor scelerisque nisi.</p>\r\n</body>\r\n</html>', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `portfolio_nome` varchar(200) NOT NULL,
  `portfolio_cliente` varchar(200) NOT NULL,
  `portfolio_data` varchar(200) DEFAULT NULL,
  `portfolio_imagem` varchar(200) DEFAULT NULL,
  `portfolio_descricao` longtext DEFAULT NULL,
  `portfolio_area1` int(11) DEFAULT NULL,
  `portfolio_url` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `portfolio_nome`, `portfolio_cliente`, `portfolio_data`, `portfolio_imagem`, `portfolio_descricao`, `portfolio_area1`, `portfolio_url`) VALUES
(1, 'News', 'Forest', '12/04/2017', '1507152628.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Pulvinar nisi dolor lacus rhoncus, purus? Et, non, risus enim rhoncus. Nunc placerat. Nunc placerat! Turpis placerat urna magnis turpis nec etiam lectus ut, turpis tempor turpis amet et? Porta risus scelerisque rhoncus, scelerisque sagittis! Ridiculus lorem sit risus ultricies. Mid? Natoque? Massa lectus dolor magna diam, porta nascetur amet nunc ut non, ultricies duis. Ac parturient sociis ac? Massa dictumst, montes turpis lacus non rhoncus ridiculus nec. Rhoncus nunc magna, tristique penatibus enim! In natoque porta turpis montes, enim nunc mattis urna magna sed lectus eu integer in, natoque turpis in vut adipiscing a enim pulvinar, pulvinar elementum porttitor, elit eu a, cras quis lundium arcu tincidunt auctor augue! Lacus porttitor in, porttitor porta a duis rhoncus augue.</p>\r\n</body>\r\n</html>', 3, 'https://www.youtube.com'),
(2, 'Visual', '', '05/10/2017', '1507216499.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Pulvinar nisi dolor lacus rhoncus, purus? Et, non, risus enim rhoncus. Nunc placerat. Nunc placerat! Turpis placerat urna magnis turpis nec etiam lectus ut, turpis tempor turpis amet et? Porta risus scelerisque rhoncus, scelerisque sagittis! Ridiculus lorem sit risus ultricies. Mid? Natoque? Massa lectus dolor magna diam, porta nascetur amet nunc ut non, ultricies duis. Ac parturient sociis ac? Massa dictumst, montes turpis lacus non rhoncus ridiculus nec. Rhoncus nunc magna, tristique penatibus enim! In natoque porta turpis montes, enim nunc mattis urna magna sed lectus eu integer in, natoque turpis in vut adipiscing a enim pulvinar, pulvinar elementum porttitor, elit eu a, cras quis lundium arcu tincidunt auctor augue! Lacus porttitor in, porttitor porta a duis rhoncus augue.</p>\r\n</body>\r\n</html>', 2, ''),
(3, 'World', 'Forest', '14/10/2017', '1507201545.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Pulvinar nisi dolor lacus rhoncus, purus? Et, non, risus enim rhoncus. Nunc placerat. Nunc placerat! Turpis placerat urna magnis turpis nec etiam lectus ut, turpis tempor turpis amet et? Porta risus scelerisque rhoncus, scelerisque sagittis! Ridiculus lorem sit risus ultricies. Mid? Natoque? Massa lectus dolor magna diam, porta nascetur amet nunc ut non, ultricies duis. Ac parturient sociis ac? Massa dictumst, montes turpis lacus non rhoncus ridiculus nec. Rhoncus nunc magna, tristique penatibus enim! In natoque porta turpis montes, enim nunc mattis urna magna sed lectus eu integer in, natoque turpis in vut adipiscing a enim pulvinar, pulvinar elementum porttitor, elit eu a, cras quis lundium arcu tincidunt auctor augue! Lacus porttitor in, porttitor porta a duis rhoncus augue.</p>\r\n</body>\r\n</html>', 1, 'https://www.youtube.com'),
(4, 'PROJETO TESTE', 'AHJKLÇ', '27/02/2019', '1551309070.jpg', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>SDHSTRH</p>\r\n</body>\r\n</html>', 5, 'ASDFGHJKL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servico`
--

CREATE TABLE `servico` (
  `servico_id` int(11) NOT NULL,
  `servico_nome` varchar(200) DEFAULT NULL,
  `servico_icon` varchar(200) DEFAULT NULL,
  `servico_descricao` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servico`
--

INSERT INTO `servico` (`servico_id`, `servico_nome`, `servico_icon`, `servico_descricao`) VALUES
(1, 'Sistemas Web', 'fa fa-bar-chart', 'Aumente a produtividade de todos os setores da sua empresa, garantindo melhor retorno e lucratividade. Tenha todas as ferramentas necessárias para a sua gestão no sistema web e facilite o dia a dia dos profissionais, focando em atividades que dão lucro.'),
(2, 'SITES', 'fa fa-desktop', 'Enquanto o marketing tradicional precisa de um investimento elevado no início, o Marketing Digital permite um investimento mais baixo e com um melhor retorno sobre esse investimento, deixe sua empresa acessível a todos.'),
(3, 'Aplicativos', 'fa fa-mobile', 'Dispomos de equipe especializada nas mais atuais tecnologias para o desenvolvimento de seu projeto de app.'),
(4, 'Consultoria', 'fa fa-instagram', 'Somos especialistas em consultoria empresarial com foco em TI e Desenvolvimento de Soluções, realizamos treinamentos, Desenvolvimento de Estratégias de alavancagem com foco em resultado.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `site`
--

CREATE TABLE `site` (
  `site_id` int(11) NOT NULL,
  `site_meta_desc` text DEFAULT NULL,
  `site_meta_palavra` varchar(200) DEFAULT NULL,
  `site_meta_titulo` varchar(200) DEFAULT NULL,
  `site_meta_autor` varchar(200) DEFAULT NULL,
  `site_analytics` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `site`
--

INSERT INTO `site` (`site_id`, `site_meta_desc`, `site_meta_palavra`, `site_meta_titulo`, `site_meta_autor`, `site_analytics`) VALUES
(1, 'CMMSOFT Soluções e Sistemas', 'site, modelo1, php, script', 'CMMSOFT', 'CMMSOFT Soluções e Sistemas', 'UA-55892535-1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `slide`
--

CREATE TABLE `slide` (
  `slide_id` int(11) NOT NULL,
  `slide_imagem` varchar(200) NOT NULL,
  `slide_nome` varchar(200) DEFAULT NULL,
  `slide_subtitulo` varchar(200) DEFAULT NULL,
  `slide_subtitulo1` varchar(200) DEFAULT NULL,
  `slide_alinha` char(50) DEFAULT NULL,
  `slide_button_text1` char(50) DEFAULT NULL,
  `slide_button_link1` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `slide`
--

INSERT INTO `slide` (`slide_id`, `slide_imagem`, `slide_nome`, `slide_subtitulo`, `slide_subtitulo1`, `slide_alinha`, `slide_button_text1`, `slide_button_link1`) VALUES
(1, '1551232913.jpg', 'A CMMSoft', 'possui uma equipe de especialistas capacitados para atender suas necessidades com base no seu negócio.', '', 'right', '', ''),
(2, '1551232390.jpg', 'Possibilitamos o crescimento do seu negócio', 'de forma gradual e exponencial com o uso das nossas tecnologias.', '', 'center', '', ''),
(3, '1551234366.jpg', 'Bem vindo à CMMSoft', 'Soluções e Sistemas', '', 'center', 'Site atual', 'cmmsoftware.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `smtp`
--

CREATE TABLE `smtp` (
  `smtp_id` int(11) NOT NULL,
  `smtp_host` varchar(200) DEFAULT NULL,
  `smtp_username` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `smtp_fromname` varchar(200) DEFAULT NULL,
  `smtp_bcc` varchar(100) DEFAULT NULL,
  `smtp_replyto` varchar(100) DEFAULT NULL,
  `smtp_port` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `smtp`
--

INSERT INTO `smtp` (`smtp_id`, `smtp_host`, `smtp_username`, `smtp_password`, `smtp_fromname`, `smtp_bcc`, `smtp_replyto`, `smtp_port`) VALUES
(1, 'mail.seudominio.com.br', 'usuario@dominio.com.br', '', 'Kit Sites', '', '', 587);

-- --------------------------------------------------------

--
-- Estrutura da tabela `social`
--

CREATE TABLE `social` (
  `social_id` int(11) NOT NULL,
  `social_titulo` varchar(200) CHARACTER SET utf8 DEFAULT '',
  `social_url` varchar(200) DEFAULT NULL,
  `social_nome` varchar(200) DEFAULT NULL,
  `social_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `social`
--

INSERT INTO `social` (`social_id`, `social_titulo`, `social_url`, `social_nome`, `social_status`) VALUES
(13, 'Dribbble', 'https://dribbble.com/', 'fa fa-dribbble', 0),
(1, 'Facebook', 'https://facebook.com', 'fa fa-facebook', 1),
(7, 'Flickr', 'https://flickr.com/', 'fa fa-flickr', 0),
(6, 'Foursquare', 'https://pt.foursquare.com/', 'fa fa-foursquare', 0),
(5, 'Google +', 'https://plus.google.com/?hl=pt-BR\r\n', 'fa fa-google-plus', 0),
(3, 'Instagram', 'https://instagram.com/', 'fa fa-instagram', 1),
(8, 'linkedIn', 'https://br.linkedin.com/', 'fa fa-linkedin', 0),
(9, 'Pinterest', 'https://br.pinterest.com/', 'fa fa-pinterest', 0),
(10, 'Skype', 'https://skype.com/pt-br/', 'fa fa-skype', 0),
(11, 'Tumblr', 'https://tumblr.com/', 'fa fa-tumblr', 0),
(2, 'Twitter', 'https://twitter.com/', 'fa fa-twitter', 0),
(12, 'Vimeo', 'https://vimeo.com/', 'fa fa-vimeo-square', 0),
(4, 'YouTube', 'https://youtube.com/user/', 'fa fa-youtube', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `usuario_nome` varchar(200) DEFAULT NULL,
  `usuario_login` varchar(200) DEFAULT NULL,
  `usuario_email` varchar(200) DEFAULT NULL,
  `usuario_senha` varchar(200) DEFAULT NULL,
  `usuario_data` varchar(200) DEFAULT NULL,
  `usuario_imagem` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_nome`, `usuario_login`, `usuario_email`, `usuario_senha`, `usuario_data`, `usuario_imagem`) VALUES
(2, 'Administrador', 'cmmsoft', 'contato@cmmsoftware.com.br', '928a0eb7c5f6f2dcb3cfd4802b77da0c', '20/01/2017', '1502194789.jpg'),
(3, 'teste', 'teste', 'teste@teste.com', '698dc19d489c4e4db73e28a713eab07b', '19/03/2019', ''),
(4, 'teste2', 'teste2', 'teste2@hotmail.com', '38851536d87701d2191990e24a7f8d4e', '19/03/2019', ''),
(5, 'teste3', 'teste3', 'teste3@hotmail.com', '507eb04c9c427e9f961e47a7204fac41', '19/03/2019', ''),
(6, 'teste4', 'teste4', 'teste4@hotmail.com', '73bf3127fb3c9791e88a4d308171fd85', '19/03/2019', ''),
(7, 'teste5', 'teste5', 'teste5@hotmail.com', '6ee7a7f22c4024cef59d25be2365a5a7', '19/03/2019', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_nome` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `video_area2` int(11) DEFAULT NULL,
  `video_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `video_imagem` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `video_views` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `video`
--

INSERT INTO `video` (`video_id`, `video_nome`, `video_area2`, `video_url`, `video_imagem`, `video_views`) VALUES
(1, 'TRABALHO EM EQUIPE', 1, 'NVGDN29LiuU', 'https://i.ytimg.com/vi/NVGDN29LiuU/hqdefault.jpg?custom=true&w=400&h=250&jpg', 11),
(2, 'VIDEO MOTIVACIONAL Eleito o Melhor 2015/2016 - Deivison Pedroza', 1, 'IAnzAWt5tCI', 'https://i.ytimg.com/vi/IAnzAWt5tCI/hqdefault.jpg?custom=true&w=400&h=250&jpg', 10),
(3, 'Ã‰ um lindo dia e eu nÃ£o posso vÃª-lo - O Cego e o PublicitÃ¡rio', 2, 'J3a3-I42AgQ', 'https://i.ytimg.com/vi/J3a3-I42AgQ/hqdefault.jpg?custom=true&w=400&h=250&jpg', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `area1`
--
ALTER TABLE `area1`
  ADD PRIMARY KEY (`area1_id`);

--
-- Indexes for table `area2`
--
ALTER TABLE `area2`
  ADD PRIMARY KEY (`area2_id`);

--
-- Indexes for table `area3`
--
ALTER TABLE `area3`
  ADD PRIMARY KEY (`area3_id`),
  ADD KEY `area3_nome` (`area3_nome`);

--
-- Indexes for table `cadastros`
--
ALTER TABLE `cadastros`
  ADD PRIMARY KEY (`cadastro_id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Indexes for table `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`comentario_id`),
  ADD KEY `fk_comentario_pagina1_idx` (`comentario_pagina`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`contato_id`);

--
-- Indexes for table `depoimento`
--
ALTER TABLE `depoimento`
  ADD PRIMARY KEY (`depoimento_id`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`equipe_id`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`foto_id`),
  ADD KEY `fk_foto_portfolio_idx` (`foto_portfolio`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`fotos_id`);

--
-- Indexes for table `icones`
--
ALTER TABLE `icones`
  ADD PRIMARY KEY (`icones_id`);

--
-- Indexes for table `icons_social`
--
ALTER TABLE `icons_social`
  ADD PRIMARY KEY (`icon_id`);

--
-- Indexes for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`mensagem_id`);

--
-- Indexes for table `modulo1`
--
ALTER TABLE `modulo1`
  ADD PRIMARY KEY (`modulo1_id`);

--
-- Indexes for table `modulo2`
--
ALTER TABLE `modulo2`
  ADD PRIMARY KEY (`modulo2_id`);

--
-- Indexes for table `modulo3`
--
ALTER TABLE `modulo3`
  ADD PRIMARY KEY (`modulo3_id`);

--
-- Indexes for table `modulo4`
--
ALTER TABLE `modulo4`
  ADD PRIMARY KEY (`modulo4_id`);

--
-- Indexes for table `modulo5`
--
ALTER TABLE `modulo5`
  ADD PRIMARY KEY (`modulo5_id`);

--
-- Indexes for table `modulo6`
--
ALTER TABLE `modulo6`
  ADD PRIMARY KEY (`modulo6_id`);

--
-- Indexes for table `modulo7`
--
ALTER TABLE `modulo7`
  ADD PRIMARY KEY (`modulo7_id`);

--
-- Indexes for table `modulo8`
--
ALTER TABLE `modulo8`
  ADD PRIMARY KEY (`modulo8_id`);

--
-- Indexes for table `modulo9`
--
ALTER TABLE `modulo9`
  ADD PRIMARY KEY (`modulo9_id`);

--
-- Indexes for table `modulo10`
--
ALTER TABLE `modulo10`
  ADD PRIMARY KEY (`modulo10_id`);

--
-- Indexes for table `modulo11`
--
ALTER TABLE `modulo11`
  ADD PRIMARY KEY (`modulo11_id`);

--
-- Indexes for table `modulo12`
--
ALTER TABLE `modulo12`
  ADD PRIMARY KEY (`modulo12_id`);

--
-- Indexes for table `modulo13`
--
ALTER TABLE `modulo13`
  ADD PRIMARY KEY (`modulo13_id`);

--
-- Indexes for table `modulo14`
--
ALTER TABLE `modulo14`
  ADD PRIMARY KEY (`modulo14_id`);

--
-- Indexes for table `modulo15`
--
ALTER TABLE `modulo15`
  ADD PRIMARY KEY (`modulo15_id`);

--
-- Indexes for table `modulo16`
--
ALTER TABLE `modulo16`
  ADD PRIMARY KEY (`modulo16_id`);

--
-- Indexes for table `modulo17`
--
ALTER TABLE `modulo17`
  ADD PRIMARY KEY (`modulo17_id`);

--
-- Indexes for table `modulo_aparencia`
--
ALTER TABLE `modulo_aparencia`
  ADD PRIMARY KEY (`modulo_aparencia_id`);

--
-- Indexes for table `pagina`
--
ALTER TABLE `pagina`
  ADD PRIMARY KEY (`pagina_id`),
  ADD KEY `fk_pagina_area_idx` (`pagina_area`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`paginas_id`),
  ADD KEY `paginas_nome` (`paginas_nome`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`),
  ADD KEY `fk_portfolio_area1_idx` (`portfolio_area1`);

--
-- Indexes for table `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`servico_id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`site_id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`social_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `area1`
--
ALTER TABLE `area1`
  MODIFY `area1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `area2`
--
ALTER TABLE `area2`
  MODIFY `area2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `area3`
--
ALTER TABLE `area3`
  MODIFY `area3_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cadastros`
--
ALTER TABLE `cadastros`
  MODIFY `cadastro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comentario`
--
ALTER TABLE `comentario`
  MODIFY `comentario_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `depoimento`
--
ALTER TABLE `depoimento`
  MODIFY `depoimento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `equipe_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `foto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `fotos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `icones`
--
ALTER TABLE `icones`
  MODIFY `icones_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=719;

--
-- AUTO_INCREMENT for table `icons_social`
--
ALTER TABLE `icons_social`
  MODIFY `icon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `mensagens`
--
ALTER TABLE `mensagens`
  MODIFY `mensagem_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo1`
--
ALTER TABLE `modulo1`
  MODIFY `modulo1_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo2`
--
ALTER TABLE `modulo2`
  MODIFY `modulo2_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo3`
--
ALTER TABLE `modulo3`
  MODIFY `modulo3_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo4`
--
ALTER TABLE `modulo4`
  MODIFY `modulo4_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo5`
--
ALTER TABLE `modulo5`
  MODIFY `modulo5_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo6`
--
ALTER TABLE `modulo6`
  MODIFY `modulo6_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo7`
--
ALTER TABLE `modulo7`
  MODIFY `modulo7_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo8`
--
ALTER TABLE `modulo8`
  MODIFY `modulo8_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo14`
--
ALTER TABLE `modulo14`
  MODIFY `modulo14_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modulo15`
--
ALTER TABLE `modulo15`
  MODIFY `modulo15_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pagina`
--
ALTER TABLE `pagina`
  MODIFY `pagina_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
  MODIFY `paginas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `servico`
--
ALTER TABLE `servico`
  MODIFY `servico_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
